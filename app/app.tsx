import "./i18n"
import "./utils/ignore-warnings"

import { NativeBaseProvider } from "native-base"
import React, { useEffect, useRef, useState } from "react"
import { Alert } from "react-native"
import { initialWindowMetrics, SafeAreaProvider } from "react-native-safe-area-context"
// This puts screens in a native ViewController or Activity. If you want fully native
// stack navigation, use `createNativeStackNavigator` in place of `createStackNavigator`:
// https://github.com/kmagiera/react-native-screens#using-native-stack-navigator
import { enableScreens } from "react-native-screens"
import SplashScreen from "react-native-splash-screen"
import { NavigationContainerRef } from "@react-navigation/native"

// eslint-disable-next-line import/no-named-default
import { ToggleStorybook } from "../storybook/toggle-storybook"
import { ToastsContainer } from "./components/base"
import { RootStore, RootStoreProvider, setupRootStore } from "./models"
import {
  canExit,
  RootNavigator,
  setRootNavigation,
  useBackButtonHandler,
  useNavigationPersistence,
} from "./navigators"
import { initFonts, theme } from "./theme"
import * as storage from "./utils/storage"
import { PinCodeScreen } from "./screens"
import { PrivacyBlur } from "./components/features"
import NetInfo from "@react-native-community/netinfo"
import { translate } from "./i18n"

enableScreens()

export const NAVIGATION_PERSISTENCE_KEY = "NAVIGATION_STATE"

/**
 * This is the root component of our app.
 */
function App() {
  const navigationRef = useRef<NavigationContainerRef>()
  const [rootStore, setRootStore] = useState<RootStore | undefined>(undefined)

  const [emptyWallets, setEmptyWallets] = useState<boolean>(true)
  const offlineModalIsVisible = useRef<boolean>(false)

  setRootNavigation(navigationRef)
  useBackButtonHandler(navigationRef, canExit)

  const { initialNavigationState, onNavigationStateChange } = useNavigationPersistence(
    storage,
    NAVIGATION_PERSISTENCE_KEY,
  )

  const showNetworkModal = () => {
    offlineModalIsVisible.current = true
    Alert.alert(translate("titles.offline"), translate("texts.offline"), [
      {
        text: translate("buttons.tryAgain"),
        onPress: () => {
          offlineModalIsVisible.current = false
          NetInfo.refresh().then((state) => {
            if (!state.isConnected && !offlineModalIsVisible.current) {
              showNetworkModal()
            }
          })
        },
      },
    ])
  }

  useEffect(() => {
    ;(async () => {
      await initFonts() // expo

      await setupRootStore().then((_rootStore) => {
        setRootStore(_rootStore)

        _rootStore.coinStore.coins.map((coin) => {
          return coin.name !== "btc"
        })

        setEmptyWallets(!!_rootStore.walletStore.mnemonic)
      })

      SplashScreen.hide()
    })()
  }, [])

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener((state) => {
      if (!state.isConnected && !offlineModalIsVisible.current) {
        showNetworkModal()
      }
    })

    return () => {
      unsubscribe()
    }
  }, [])

  if (!rootStore) return null

  return (
    <ToggleStorybook>
      <RootStoreProvider value={rootStore}>
        <NativeBaseProvider theme={theme}>
          <SafeAreaProvider initialMetrics={initialWindowMetrics}>
            <RootNavigator
              ref={navigationRef}
              initialState={initialNavigationState}
              showOnboardingScreen={emptyWallets}
              onStateChange={onNavigationStateChange}
            />

            {/* <PinCodeScreen /> */}
            <ToastsContainer />
            {/* <PrivacyBlur /> */}
          </SafeAreaProvider>
        </NativeBaseProvider>
      </RootStoreProvider>
    </ToggleStorybook>
  )
}

export default App
