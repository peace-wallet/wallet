import { Coin, Fee } from "../../../models"

export type FeeSelectProps = {
  fees: Fee[]
  value: Fee
  coin: Coin
  onChange?(fee: any): void
}
