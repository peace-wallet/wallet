import { Popover, useTheme } from "native-base"
import React, { FC, useRef } from "react"
import { Dimensions, Platform, StyleSheet } from "react-native"
import Carousel from "react-native-reanimated-carousel"
import LinearGradient from "react-native-linear-gradient"

import { useNavigation } from "@react-navigation/core"
import numeral from "numeral"
import { Coin, useStores } from "../../../models"
import { Box, Button, HStack, Icon, Pressable, Spacer, Text, VStack } from "../../base"
import { CoinsCarouselProps } from "./coins-carousel.props"

export const CoinsCarousel: FC<CoinsCarouselProps> = (props) => {
  const {
    coins,
    initialIndex = 0,
    onSnapToItem = () => null,
    onLockedBalancePress = () => null,
  } = props
  const { balanceStore } = useStores()

  const navigation = useNavigation()
  const { colors } = useTheme()

  const carouselRef = useRef(null)

  let timeout

  const SLIDER_WIDTH = Dimensions.get("window").width

  const handleSnapToItem = async (index) => {
    const _index = index >= coins.length ? index - 2 : index

    clearTimeout(timeout)
    timeout = setTimeout(() => {
      onSnapToItem(coins[_index])
    }, 150)
  }

  const onReceive = async () => {
    navigation.navigate("receiveStack", {
      screen: "receiveQR",
    })
  }

  const onSend = async (coin: Coin) => {
    navigation.navigate("sendStack", {
      screen: "sendCreate",
      params: {
        coinName: coin.name,
      },
    })
  }

  const renderCarouselItem = (index) => {
    const _index = index >= coins.length ? index - 2 : index

    const coin = coins[_index]

    const availableBalance = balanceStore.balanceByCoin(coin).available
    const lockedBalance = balanceStore.balanceByCoin(coin).locked

    const formattedAvailableBalance =
      availableBalance > 10000 ? numeral(availableBalance).format("0.00a") : availableBalance
    const formattedLockedBalance =
      lockedBalance > 10000 ? numeral(lockedBalance).format("0.00a") : lockedBalance

    return (
      <Box
        height={200}
        rounded="xl"
        ml={2}
        mr={_index === coins.length - 1 ? 2 : 1.5}
        overflow="hidden"
      >
        <VStack flex={1}>
          <LinearGradient
            colors={[colors.coins[coin.name][100], colors.coins[coin.name][300]]}
            locations={[0, 0.8]}
            useAngle={true}
            angle={_index === 1 ? 52 : 130}
            style={styles.gradient}
          >
            <Box flex={1} p="2">
              <VStack flex={1}>
                <HStack px="2" pt="2">
                  <Text text={coin.title} color="primary.500" opacity={0.6} />
                </HStack>

                <Spacer />

                <VStack px="2">
                  <HStack>
                    {availableBalance > 10000 && Platform.OS === "ios" ? (
                      <Popover
                        trigger={(triggerProps) => {
                          return (
                            <Pressable {...triggerProps}>
                              <Text
                                text={`${formattedAvailableBalance}`}
                                color="primary.500"
                                fontSize={28}
                              />
                            </Pressable>
                          )
                        }}
                      >
                        <Popover.Content>
                          <Popover.Arrow />
                          <Popover.Body>
                            <Text text={`${availableBalance}`} fontSize={14} />
                          </Popover.Body>
                        </Popover.Content>
                      </Popover>
                    ) : (
                      <Text
                        text={`${formattedAvailableBalance}`}
                        color="primary.500"
                        fontSize={28}
                      />
                    )}
                  </HStack>

                  {lockedBalance > 0 && (
                    <Pressable onPress={() => onLockedBalancePress(coin)}>
                      <HStack alignItems="center" mt={-1} alignContent="center" space={1}>
                        <Text
                          textTx="texts.includingLocked"
                          textTxOptions={{
                            balance: formattedLockedBalance,
                          }}
                          secondary
                          lineHeight={16}
                        />
                        <Icon.ChevronRight size={4} opacity={0.8} />
                      </HStack>
                    </Pressable>
                  )}
                </VStack>

                <Spacer />

                <HStack space="xs">
                  <Button
                    flex={1}
                    textTx="buttons.receive"
                    variant="outline"
                    endIcon={<Icon.ArrowBottomLeft color="primary.500" />}
                    _light={{ backgroundColor: "whiteAlpha.200", borderColor: "whiteAlpha.100" }}
                    _dark={{ backgroundColor: "whiteAlpha.200", borderColor: "whiteAlpha.100" }}
                    _text={{ color: "primary.500" }}
                    onPress={onReceive}
                  />
                  <Button
                    flex={1}
                    textTx="buttons.send"
                    variant="outline"
                    endIcon={<Icon.ArrowTopRight color="primary.500" />}
                    _light={{ backgroundColor: "whiteAlpha.200", borderColor: "whiteAlpha.100" }}
                    _dark={{ backgroundColor: "whiteAlpha.200", borderColor: "whiteAlpha.100" }}
                    _text={{ color: "primary.500" }}
                    onPress={() => onSend(coin)}
                  />
                </HStack>
              </VStack>
            </Box>
          </LinearGradient>
        </VStack>
      </Box>
    )
  }

  return (
    <VStack>
      <Carousel
        defaultIndex={initialIndex}
        ref={carouselRef}
        width={SLIDER_WIDTH}
        height={200}
        data={[...new Array(coins.length).keys()]}
        mode="parallax"
        modeConfig={{
          parallaxScrollingScale: 0.9,
          parallaxScrollingOffset: 60,
        }}
        renderItem={({ index }) => renderCarouselItem(index)}
        onSnapToItem={(index) => handleSnapToItem(index)}
      />
    </VStack>
  )
}

const styles = StyleSheet.create({
  gradient: {
    height: "100%",
  },
})
