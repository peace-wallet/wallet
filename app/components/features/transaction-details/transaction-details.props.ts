import { Coin } from "../../../models/coin/coin"

export type TransactionDetailsProps = {
  coin: Coin
  address: string
  sourceShardID: number
  destinationShardID?: number
  sourceAmount: number
  destinationAmount?: number
  transactionFee?: number
  hash?: string
  confirmations?: number
  blockNumber?: number
  exchangeAgentFee?: number
  exchangeAgentUrl?: string
  direction?: "out" | "in" | string
  lockTransactionHash?: string
}
