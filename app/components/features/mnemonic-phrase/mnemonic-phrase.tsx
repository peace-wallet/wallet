import { observer } from "mobx-react-lite"
import text from "native-base/src/theme/components/text"
import React, { FC } from "react"
import { StyleSheet, TextBase } from "react-native"

import { Box, HStack, Text, VStack } from "../../base"
import { MnemonicPhraseProps } from "./mnemonic-phrase.props"

export const MnemonicPhrase: FC<MnemonicPhraseProps> = observer((props) => {
  const { mnemonic } = props

  const renderPhrases = () => {
    if (!mnemonic) return <></>

    const rows = []
    let cols = []

    for (let i = 0; i <= mnemonic.split(" ").length; i++) {
      const isEndOfLine = (i + 1) % 3 === 0

      const col = (
        <Box
          flex={1}
          key={`col-${i}`}
          rounded="lg"
          py="3"
          style={styles.tile}
          _light={{
            backgroundColor: "white",
            borderColor: "primaryAlpha.50",
            _android: {
              borderColor: "primaryAlpha.100",
            },
          }}
          _dark={{
            backgroundColor: "darkGray.500",
            borderColor: "whiteAlpha.50",
            _android: {
              borderColor: "whiteAlpha.100",
            },
          }}
        >
          <HStack alignItems="center" justifyContent="center" space="xs">
            <Text
              text={`${i + 1}.`}
              _light={{
                color: "primaryAlpha.500",
              }}
              _dark={{
                color: "whiteAlpha.500",
              }}
            />
            <Text text={mnemonic.split(" ")[i]} testID={`mnemonic-words-${i}`} />
          </HStack>
        </Box>
      )

      cols.push(col)

      if (isEndOfLine) {
        rows.push(
          <HStack space="xs" key={`row-${i}`}>
            {cols}
          </HStack>,
        )
        cols = []
      }
    }

    return rows
  }

  return <VStack space="xs">{renderPhrases()}</VStack>
})

const styles = StyleSheet.create({
  tile: {
    borderWidth: 1,
  },
})
