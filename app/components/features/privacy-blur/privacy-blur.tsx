/* eslint-disable react-native/no-inline-styles */
import { BlurView } from "@react-native-community/blur"
import { useColorMode } from "native-base"
import React, { useEffect, useRef, useState } from "react"
import { Animated, AppState, Platform, View } from "react-native"

export const PrivacyBlur = () => {
  const { colorMode } = useColorMode()

  const appState = useRef(AppState.currentState)
  const blurViewFadeAnim = useRef(new Animated.Value(1)).current

  const [blurViewIsVisible, setBlurViewIsVisible] = useState(false)

  const handleAppStateChange = (nextAppState) => {
    if (appState.current.match(/inactive|background/) && nextAppState === "active") {
      fadeOut()
      setTimeout(() => {
        setBlurViewIsVisible(false)
      }, 250)
    } else if (appState.current === "active" && nextAppState.match(/inactive|background/)) {
      setBlurViewIsVisible(true)
      fadeIn()
    }

    appState.current = nextAppState
  }

  const fadeIn = () => {
    if (Platform.OS === "android") return

    Animated.timing(blurViewFadeAnim, {
      toValue: 1,
      duration: 150,
      useNativeDriver: false,
    }).start()
  }

  const fadeOut = () => {
    if (Platform.OS === "android") return

    Animated.timing(blurViewFadeAnim, {
      toValue: 0,
      duration: 250,
      useNativeDriver: false,
    }).start()
  }

  const renderBlurView = () => (
    <BlurView
      style={{
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
      }}
      blurType={colorMode === "light" ? "light" : "dark"}
      blurAmount={20}
      reducedTransparencyFallbackColor={colorMode === "light" ? "white" : "black"}
    />
  )

  useEffect(() => {
    AppState.addEventListener("change", handleAppStateChange)

    return () => {
      AppState.removeEventListener("change", handleAppStateChange)
    }
  }, [])

  return blurViewIsVisible ? (
    Platform.OS === "ios" ? (
      <Animated.View
        style={{
          opacity: blurViewFadeAnim,
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        }}
      >
        {renderBlurView()}
      </Animated.View>
    ) : (
      <View
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        }}
      >
        {renderBlurView()}
      </View>
    )
  ) : null
}
