import { Center as NBCenter } from "native-base"
import React, { FC } from "react"

import { CenterProps } from "./types"

export const Center: FC<CenterProps> = (props) => {
  return <NBCenter {...props} />
}
