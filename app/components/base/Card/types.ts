import { IBoxProps } from "native-base"

export type CardProps = IBoxProps & {
  error?: boolean
}
