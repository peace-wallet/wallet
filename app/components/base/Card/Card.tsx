import { Box, useColorModeValue } from "native-base"
import React, { FC } from "react"
import { StyleSheet } from "react-native"

import { CardProps } from "./types"

export const Card: FC<CardProps> = (props) => {
  const { rounded = "xl", p = 4, error = false, children, ...rest } = props

  return (
    <Box
      rounded={rounded}
      shadow="1"
      p={p}
      _light={{ backgroundColor: "white" }}
      _dark={{ backgroundColor: "darkGray.500" }}
      {...rest}
    >
      {children}
      {error && (
        <Box
          style={styles.borderOverlay}
          rounded={rounded}
          borderWidth={1}
          pointerEvents="none"
          borderColor={error ? "red.500" : useColorModeValue("white", "darkGray.500")}
        />
      )}
    </Box>
  )
}

const styles = StyleSheet.create({
  borderOverlay: {
    bottom: 0,
    left: 0,
    position: "absolute",
    right: 0,
    top: 0,
  },
})
