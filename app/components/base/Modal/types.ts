import { ModalizeProps } from "react-native-modalize"

export type ModalProps = ModalizeProps & {
  isVisible: boolean
}
