import { Spacer as NBSpacer } from "native-base"
import React, { FC } from "react"

import { SpacerProps } from "./types"

export const Spacer: FC<SpacerProps> = (props) => {
  return <NBSpacer {...props} />
}
