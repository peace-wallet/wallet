import { HStack as NBHStack } from "native-base"
import React, { FC } from "react"

import { HStackProps } from "./types"

export const HStack: FC<HStackProps> = (props) => {
  return <NBHStack {...props} />
}
