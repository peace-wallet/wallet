import { VStack as NBVStack } from "native-base"
import React, { FC } from "react"

import { VStackProps } from "./types"

export const VStack: FC<VStackProps> = (props) => {
  return <NBVStack {...props} />
}
