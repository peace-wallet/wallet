import { Switch as NBSwitch } from "native-base"
import React, { FC } from "react"

import { SwitchProps } from "./types"

export const Switch: FC<SwitchProps> = (props) => {
  return <NBSwitch {...props} />
}
