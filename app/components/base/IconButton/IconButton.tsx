import { IconButton as NBIconButton } from "native-base"
import React, { FC } from "react"

import { IconButtonProps } from "./types"

export const IconButton: FC<IconButtonProps> = (props) => {
  const { _pressed = { opacity: 0.8 }, ...rest } = props
  return <NBIconButton _pressed={_pressed} {...rest} />
}
