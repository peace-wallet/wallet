import { IIconButtonProps } from "native-base"

export type IconButtonProps = IIconButtonProps
