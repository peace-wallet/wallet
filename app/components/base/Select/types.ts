import { ISelectItemProps, ISelectProps } from "native-base"
import { MutableRefObject } from "react"

import { TxKeyPath } from "../../../i18n"

export type SelectProps = ISelectProps & {
  /**
   * Placeholder text which is looked up via i18n.
   */
  placeholderTx?: TxKeyPath

  /**
   * Optional options to pass to i18n. Useful for interpolation
   * as well as explicitly setting locale or translation fallbacks.
   */
  placeholderTxOptions?: I18n.TranslateOptions
}

export type SelectComponentType = ((
  props: ISelectProps & SelectProps & { ref?: any },
) => JSX.Element) & {
  Item: React.MemoExoticComponent<
    (
      props: ISelectItemProps & {
        ref?: MutableRefObject<any>
      },
    ) => JSX.Element
  >
}
