import { Divider as NBDivider } from "native-base"
import React, { FC } from "react"

import { DividerProps } from "./types"

export const Divider: FC<DividerProps> = (props) => {
  return <NBDivider {...props} />
}
