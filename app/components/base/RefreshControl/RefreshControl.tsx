import { useColorModeValue, useToken } from "native-base"
import React, { FC } from "react"
import { RefreshControl as NRefreshControl } from "react-native"

import { translate } from "../../../i18n"
import { RefreshControlProps } from "./types"

export const RefreshControl: FC<RefreshControlProps> = (props) => {
  const { title, titleTx, titleTxOptions, size = 24, refreshing, onRefresh, ...rest } = props

  const translatedTitle = title || (titleTx ? translate(titleTx, titleTxOptions) : "")

  return (
    <NRefreshControl
      size={size}
      refreshing={refreshing}
      onRefresh={onRefresh}
      title={translatedTitle}
      titleColor={useToken("colors", useColorModeValue("primary.500", "white"))}
      colors={[useToken("colors", useColorModeValue("primaryAlpha.500", "primaryAlpha.500"))]}
      tintColor={useToken("colors", useColorModeValue("primaryAlpha.500", "whiteAlpha.500"))}
      {...rest}
    />
  )
}
