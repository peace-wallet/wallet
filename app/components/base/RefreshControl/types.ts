import { RefreshControlProps as NRefreshControlProps } from "react-native"

import { TxKeyPath } from "../../../i18n"

export type RefreshControlProps = NRefreshControlProps & {
  title?: string
  titleTx?: TxKeyPath
  titleTxOptions?: I18n.TranslateOptions
}
