import { ITextProps } from "native-base"

import { I18NComponentProps } from "../../../i18n"

export type TextProps = ITextProps &
  I18NComponentProps & {
    align?: "left" | "center" | "right"
    secondary?: boolean
  }
