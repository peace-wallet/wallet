import { useColorModeValue } from "native-base"
import React, { FC, useEffect, useRef } from "react"
import { Animated, Platform, StyleSheet, TouchableOpacity } from "react-native"
import { useSafeAreaInsets } from "react-native-safe-area-context"

import { Box, HStack } from "../"
import { theme } from "../../../theme"
import { Icon } from "../Icon"
import { TabBarProps } from "./types"

const isIOS = Platform.OS === "ios"

export const BottomTabBar: FC<TabBarProps> = (props) => {
  const { state, descriptors, navigation } = props

  const { tabBarVisible } = descriptors[state.routes[state.index].key].options

  const insets = useSafeAreaInsets()
  const tabBarPositionY = useRef(new Animated.Value(0)).current

  useEffect(() => {
    Animated.spring(tabBarPositionY, {
      toValue: tabBarVisible ? 0 : 100,
      speed: 14,
      useNativeDriver: true,
    }).start()
  }, [tabBarVisible])

  const onPress = (route: any, isFocused: boolean) => {
    const event = navigation.emit({
      type: "tabPress",
      target: route.key,
      canPreventDefault: true,
    })

    if (!isFocused && !event.defaultPrevented) {
      navigation.navigate(route.name)
    }
  }

  const onLongPress = (route: any) => {
    navigation.emit({
      type: "tabLongPress",
      target: route.key,
    })
  }

  const renderTabIcon = (isFocused: boolean, routeName: any): React.ReactNode => {
    let icon = null

    switch (routeName) {
      case "balance":
        icon = (
          <Icon.Wallet
            size={7}
            _light={{ color: isFocused ? "primary.500" : "primary.100" }}
            _dark={{ color: isFocused ? "white" : "whiteAlpha.500" }}
          />
        )
        break
      case "exchange":
        icon = (
          <Icon.Wallet
            size={7}
            _light={{ color: isFocused ? "primary.500" : "primary.100" }}
            _dark={{ color: isFocused ? "white" : "whiteAlpha.500" }}
          />
        )
        break
      case "settings":
        icon = (
          <Icon.Settings
            size={7}
            _light={{ color: isFocused ? "primary.500" : "primary.100" }}
            _dark={{ color: isFocused ? "white" : "whiteAlpha.500" }}
          />
        )
        break

      default:
        break
    }

    return icon
  }

  return (
    <Animated.View
      style={[
        styles.container,
        {
          bottom: insets.bottom || theme.space[2],
          transform: [
            {
              translateY: tabBarPositionY,
            },
          ],
        },
      ]}
    >
      <Box style={styles.tabBar} bgColor={useColorModeValue("white", "dark.500")}>
        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key]
          const isFocused = state.index === index

          return (
            <TouchableOpacity
              key={`tab-${index}`}
              style={styles.tab}
              activeOpacity={0.8}
              accessibilityRole="button"
              accessibilityState={isFocused ? { selected: true } : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={() => onPress(route, isFocused)}
              onLongPress={() => onLongPress(route)}
            >
              <HStack alignItems="center" justifyContent="center">
                {renderTabIcon(isFocused, route.name)}
              </HStack>
            </TouchableOpacity>
          )
        })}
      </Box>
    </Animated.View>
  )
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 28,
    bottom: theme.space[16],
    height: 56,
    left: theme.space[16],
    overflow: isIOS ? "visible" : "hidden",
    position: "absolute",
    right: theme.space[16],
    ...theme.shadows[2],
  },
  tab: {
    alignContent: "center",
    alignItems: "center",
    flex: 1,
    height: "100%",
    justifyContent: "center",
  },
  tabBar: {
    alignItems: "center",
    borderRadius: 28,
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    overflow: "hidden",
    paddingHorizontal: theme.space[6],
  },
})
