import { I18NComponentProps } from "../../../i18n"

export type LinkProps = I18NComponentProps & {
  url: string
}
