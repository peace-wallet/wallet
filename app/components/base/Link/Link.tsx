import React, { FC } from "react"
import { Linking } from "react-native"

import { Pressable } from "../Pressable"
import { Text } from "../Text"
import { LinkProps } from "./types"

export const Link: FC<LinkProps> = (props) => {
  const { text, textTx, textTxOptions, url } = props

  const onOpen = async () => {
    const supported = await Linking.canOpenURL(url)

    if (supported) {
      await Linking.openURL(url)
      return
    }

    try {
      await Linking.openURL(url)
    } catch {
      console.log("cant open url", url)
    }
  }

  return (
    <Pressable onPress={onOpen}>
      <Text
        text={text}
        textTx={textTx}
        textTxOptions={textTxOptions}
        numberOfLines={1}
        ellipsizeMode="middle"
        underline
      />
    </Pressable>
  )
}
