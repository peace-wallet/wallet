import { IIconProps } from "native-base"

export type IconProps = IIconProps

type IconType = React.MemoExoticComponent<
  React.ForwardRefExoticComponent<IIconProps & React.RefAttributes<unknown>>
>

export type IconComponentType = ((props: IIconProps) => JSX.Element) & {
  ArrowLeft: IconType
  ArrowTopRight: IconType
  ArrowBottomLeft: IconType
  ChevronLeft: IconType
  ChevronRight: IconType
  ChevronTop: IconType
  ChevronDown: IconType
  Close: IconType
  Copy: IconType
  Paste: IconType
  Phone: IconType
  Refresh: IconType
  Info: IconType
  Checkmark: IconType
  ScanQR: IconType
  Send: IconType
  Wallet: IconType
  Settings: IconType
  Share: IconType
  Logout: IconType
  Lock: IconType
  ExchangeCircle: IconType
  Fingerprint: IconType
}
