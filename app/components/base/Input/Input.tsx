import { Input as NBInput } from "native-base"
import React, { FC } from "react"

import { translate } from "../../../i18n"
import { InputProps } from "./types"

export const Input: FC<InputProps> = (props) => {
  const {
    placeholder,
    placeholderTx,
    placeholderTxOptions,
    color,
    placeholderTextColor,
    error,
    ...rest
  } = props

  const translatedPlaceholder =
    placeholder || (placeholderTx ? translate(placeholderTx, placeholderTxOptions) : "")

  return (
    <NBInput
      color={error ? "red.500" : color}
      placeholder={translatedPlaceholder}
      placeholderTextColor={error ? "red.400" : placeholderTextColor}
      {...rest}
    />
  )
}
