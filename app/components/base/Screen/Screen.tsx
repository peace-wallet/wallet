import { Observer } from "mobx-react-lite"
import { Box, Container, useColorMode, useColorModeValue, useTheme } from "native-base"
import React, { useState } from "react"
import { Platform, StatusBar, StyleSheet, View } from "react-native"
import {
  KeyboardAwareFlatList,
  KeyboardAwareScrollView
} from "react-native-keyboard-aware-scroll-view"
import { useSafeAreaInsets } from "react-native-safe-area-context"

import { useStores } from "../../../models"
import { theme } from "../../../theme"
import { Text } from "../Text"
import {
  ScreenContentProps,
  ScreenFlatListContentProps,
  ScreenProps,
  ScreenScrollViewContentProps
} from "./types"

export const Screen = (props: ScreenProps) => {
  const { header, children, ...rest } = props
  const { colors } = useTheme()
  const { colorMode } = useColorMode()
  const rootStore = useStores()

  const insets = useSafeAreaInsets()

  const insetStyle = {
    paddingTop: props.unsafeTop ? 0 : insets.top,
    paddingBottom: props.unsafeBottom ? 0 : insets.bottom,
  }

  return (
    <View style={styles.screen} {...rest}>
      <StatusBar
        animated={true}
        barStyle={colorMode === "light" ? "dark-content" : "light-content"}
        backgroundColor={colorMode === "light" ? colors.offWhite : colors.dark[500]}
      />

      <Observer>
        {() =>
          rootStore.networkName === "testnet" && (
            <Box style={[styles.badge, { top: insets.top - 4 }]} backgroundColor="red.500">
              <Text
                textTx="texts.testnet"
                numberOfLines={1}
                style={styles.badgeText}
                color="white"
              />
            </Box>
          )
        }
      </Observer>

      <Container flex={1} alignItems="stretch" style={insetStyle}>
        {header}
        {children}
      </Container>
    </View>
  )
}

export const ScreenContent = (props: ScreenContentProps) => {
  const {
    children,
    style,
    paddingLess,
    extraScrollHeight = 64,
    scrollEnabled = false,
    showsVerticalScrollIndicator = false,
    ...rest
  } = props
  const insets = useSafeAreaInsets()

  const paddingStyle = {
    padding: paddingLess ? 0 : theme.space[2],
    paddingBottom: paddingLess ? 0 : insets.bottom > 0 ? 0 : theme.space[2],
  }

  return (
    <KeyboardAwareScrollView
      contentContainerStyle={[!scrollEnabled && styles.flex, paddingStyle, style]}
      extraScrollHeight={extraScrollHeight}
      scrollEnabled={scrollEnabled}
      showsVerticalScrollIndicator={showsVerticalScrollIndicator}
      {...rest}
    >
      {children}
    </KeyboardAwareScrollView>
  )
}

export const ScreenScrollViewContent = (props: ScreenScrollViewContentProps) => {
  const {
    children,
    footer,
    style,
    paddingLess,
    extraScrollHeight,
    scrollEnabled = true,
    showsVerticalScrollIndicator = false,
    ...rest
  } = props

  const insets = useSafeAreaInsets()

  const defaultExtraScrollHeight = Platform.OS === "android" ? (footer ? 88 : 24) : 64

  const [footerHeight, setFooterHeight] = useState(0)

  const paddingStyle = {
    padding: paddingLess ? 0 : theme.space[2],
    paddingBottom:
      (paddingLess ? 0 : insets.bottom > 0 ? 0 : theme.space[2]) +
      (footerHeight ? footerHeight + theme.space[2] : 0),
  }

  const bottomInsetsStyle = {
    bottom: insets.bottom > 0 ? insets.bottom : theme.space[2],
  }

  const handleFooterLayout = ({ nativeEvent }) => setFooterHeight(nativeEvent.layout.height)

  return (
    <>
      <KeyboardAwareScrollView
        enableOnAndroid={true}
        contentContainerStyle={[!scrollEnabled && styles.flex, paddingStyle, style]}
        extraScrollHeight={extraScrollHeight ?? defaultExtraScrollHeight}
        scrollEnabled={scrollEnabled}
        showsVerticalScrollIndicator={showsVerticalScrollIndicator}
        {...rest}
      >
        {children}
      </KeyboardAwareScrollView>
      {footer && (
        <Box
          style={[styles.screenFooter, bottomInsetsStyle]}
          shadow="1"
          bgColor={useColorModeValue("offWhite", "dark.500")}
          rounded="lg"
          onLayout={handleFooterLayout}
        >
          {footer}
        </Box>
      )}
    </>
  )
}

export const ScreenFlatListContent = (props: ScreenFlatListContentProps) => {
  const {
    style,
    paddingLess,
    scrollEnabled = true,
    showsVerticalScrollIndicator = false,
    ...rest
  } = props

  const insets = useSafeAreaInsets()

  const paddingStyle = {
    padding: paddingLess ? 0 : theme.space[2],
    paddingBottom: paddingLess ? 0 : insets.bottom > 0 ? 0 : theme.space[2],
  }

  return (
    <KeyboardAwareFlatList
      contentContainerStyle={[!scrollEnabled && styles.flex, paddingStyle, style]}
      extraScrollHeight={64}
      showsVerticalScrollIndicator={showsVerticalScrollIndicator}
      {...rest}
    />
  )
}

const styles = StyleSheet.create({
  badge: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    left: 0,
    paddingHorizontal: theme.space[3],
    paddingVertical: 0,
    position: "absolute",
    right: 0,
    zIndex: 9999,
  },
  badgeText: {
    fontSize: 10,
    lineHeight: 14,
  },
  flex: {
    flex: 1,
  },
  screen: {
    flex: 1,
    height: "100%",
  },
  screenFooter: {
    left: theme.space[2],
    position: "absolute",
    right: theme.space[2],
  },
})
