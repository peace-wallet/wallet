import { ViewProps } from "react-native"
import {
  KeyboardAwareFlatListProps,
  KeyboardAwareScrollViewProps
} from "react-native-keyboard-aware-scroll-view"

import { HeaderProps } from "../Header"

export type ScreenProps = ViewProps & {
  /**
   * Children components.
   */
  children?: React.ReactNode

  /**
   * Header component.
   */
  header?: React.ReactElement<HeaderProps>

  /**
   * Should we not wrap in SafeAreaView? Defaults to false.
   */
  unsafeTop?: boolean

  /**
   * Should we not wrap in SafeAreaView? Defaults to false.
   */
  unsafeBottom?: boolean

  /**
   * Layout level
   */
  level?: "1" | "2" | "3" | "4"
}

export type ScreenContentProps = KeyboardAwareScrollViewProps & {
  /**
   * Children components.
   */
  children?: React.ReactNode

  /**
   * Should we have paddings? Defaults to false.
   */
  paddingLess?: boolean
}

export type ScreenScrollViewContentProps = KeyboardAwareScrollViewProps & {
  /**
   * Children components.
   */
  children?: React.ReactNode

  /**
   * Footer component.
   */
  footer?: React.ReactNode

  /**
   * Should we have paddings? Defaults to false.
   */
  paddingLess?: boolean
}

export type ScreenFlatListContentProps = KeyboardAwareFlatListProps<any> & {
  /**
   * Should we have paddings? Defaults to false.
   */
  paddingLess?: boolean
}
