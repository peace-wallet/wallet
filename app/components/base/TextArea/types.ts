import { ITextAreaProps } from "native-base/lib/typescript/components/primitives/TextArea"

import { TxKeyPath } from "../../../i18n"

export type TextAreaProps = ITextAreaProps & {
  error?: boolean
  /**
   * Placeholder text which is looked up via i18n.
   */
  placeholderTx?: TxKeyPath

  /**
   * Optional options to pass to i18n. Useful for interpolation
   * as well as explicitly setting locale or translation fallbacks.
   */
  placeholderTxOptions?: I18n.TranslateOptions
}
