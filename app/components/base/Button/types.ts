import { IButtonProps } from "native-base"
import { IButtonGroupProps } from "native-base/lib/typescript/components/primitives/Button"

import { I18NComponentProps } from "../../../i18n"

export type ButtonProps = I18NComponentProps & IButtonProps

export type ButtonComponentType = ((
  props: IButtonProps & I18NComponentProps & { ref?: any },
) => JSX.Element) & {
  Group: React.MemoExoticComponent<(props: IButtonGroupProps & { ref?: any }) => JSX.Element>
}
