import Env from "./env"

export const { MAINNET_API_URL, TESTNET_API_URL } = Env

export default Env
