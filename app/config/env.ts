interface IEnv {
  MAINNET_API_URL: string
  TESTNET_API_URL: string
}

const Env: IEnv = {
  MAINNET_API_URL: "https://api.wallet.jax.net/api/v1",
  TESTNET_API_URL: "https://api-testnet.wallet.jax.net/api/v1",

  // MAINNET_API_URL: "https://wallet-testing.jaxdevz.space/api/v1",
  // TESTNET_API_URL: "https://wallet-testing.jaxdevz.space/api/v1",
}

export default Env
