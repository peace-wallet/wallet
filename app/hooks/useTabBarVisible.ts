import { useNavigation } from "@react-navigation/native"

export const useTabBarVisible = (visible = true) => {
  const navigation = useNavigation()

  navigation.addListener("focus", () => {
    const parent = navigation.dangerouslyGetParent()
    parent.setOptions({
      tabBarVisible: visible,
    })
  })
}
