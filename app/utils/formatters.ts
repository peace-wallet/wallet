export const CoinsFormatter = {
  toBiggestUnits: (coinName: string, value: number): number => {
    switch (coinName) {
      case "btc":
        return Number((value / 10 ** 8).toFixed(8))
      case "jax":
        return Number((value / 10 ** 4).toFixed(4))
      case "jxn":
        return Number((value / 10 ** 8).toFixed(8))
      default:
        return value
    }
  },
  toSmallestUnits: (coinName: string, value: number): number => {
    switch (coinName) {
      case "btc":
        return Math.round(value * 10 ** 8)
      case "jax":
        return Math.round(value * 10 ** 4)
      case "jxn":
        return Math.round(value * 10 ** 8)
      default:
        return value
    }
  },
}
