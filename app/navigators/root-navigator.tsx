import { useColorModeValue, useToken } from "native-base"
/**
 * The root navigator is used to switch between major navigation flows of your app.
 * Generally speaking, it will contain an auth flow (registration, login, forgot password)
 * and a "main" flow (which is contained in your MainNavigator) which the user
 * will use once logged in.
 */
import React from "react"
import { Host } from "react-native-portalize"

import {
  DefaultTheme,
  LinkingOptions,
  NavigationContainer,
  NavigationContainerRef,
} from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"

import { OnboardingStackNavigator, TabsStackNavigator } from "./stacks"
import { Linking } from "react-native"

/**
 * This type allows TypeScript to know what routes are defined in this navigator
 * as well as what properties (if any) they might take when navigating to them.
 *
 * We recommend using MobX-State-Tree store(s) to handle state rather than navigation params.
 *
 * For more information, see this documentation:
 *   https://reactnavigation.org/docs/params/
 *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
 */
export type RootParamList = {
  onboardingStack: undefined
  tabsStack: undefined
}

type RootNavigatorProps = {
  showOnboardingScreen: boolean
} & Partial<React.ComponentProps<typeof NavigationContainer>>

const Stack = createStackNavigator<RootParamList>()

const RootStack = ({ showOnboardingScreen }: RootNavigatorProps) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={showOnboardingScreen ? "onboardingStack" : "tabsStack"}
    >
      <Stack.Screen name="onboardingStack" component={OnboardingStackNavigator} />
      <Stack.Screen name="tabsStack" component={TabsStackNavigator} />
    </Stack.Navigator>
  )
}

export const RootNavigator = React.forwardRef<NavigationContainerRef, RootNavigatorProps>(
  (props, ref) => {
    const color = useToken("colors", useColorModeValue("offWhite", "dark.500"))

    const linking: LinkingOptions = {
      prefixes: ["peacewallet://"],
      getInitialURL: async () => {
        const url = await Linking.getInitialURL()

        if (url != null) {
          console.log("url 0", url)
          return url
        }
      },
      config: {
        screens: {
          tabsStack: {
            // initialRouteName: "balance",
            screens: {
              balance: {
                initialRouteName: "balance",
                screens: {
                  balance: "balance",
                  sendStack: {
                    // path: "",
                    // initialRouteName: "balance",
                    screens: {
                      sendCreate: "send/:coinName/:address/:amount?/:shardID?",
                    },
                  },
                },
              },
            },
          },
        },
      },
    }

    return (
      <NavigationContainer
        {...props}
        ref={ref}
        theme={{
          dark: true,
          colors: {
            ...DefaultTheme.colors,
            background: color,
            card: color,
          },
        }}
        linking={linking}
      >
        <Host>
          <RootStack showOnboardingScreen={props.showOnboardingScreen} />
        </Host>
      </NavigationContainer>
    )
  },
)

RootNavigator.displayName = "RootNavigator"

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 *
 * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
 */
const exitRoutes = ["onboarding", "balance"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
