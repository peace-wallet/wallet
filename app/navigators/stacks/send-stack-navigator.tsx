import React from "react"

import { createStackNavigator } from "@react-navigation/stack"

import { useTabBarVisible } from "../../hooks"
import {
  SendCreateScreen,
  SendCreateScreenParams,
  SendCrossShardProcessingScreen,
  SendCrossShardProcessingScreenParams,
  SendCrossShardRefundScreenParams,
  SendScanQRScreen,
  SendScanQRScreenParams,
  SentCrossShardRefundScreen,
  SentSuccessfullyScreen,
  SentSuccessfullyScreenParams,
} from "../../screens"

export type SendNavigatorParamList = {
  sendCreate: SendCreateScreenParams
  sentSuccessfully: SentSuccessfullyScreenParams
  sendScanQR: SendScanQRScreenParams
  sendCrossShardRefund: SendCrossShardRefundScreenParams
  sendCrossShardProcessing: SendCrossShardProcessingScreenParams
}

const Stack = createStackNavigator<SendNavigatorParamList>()

export const SendStackNavigator = () => {
  useTabBarVisible(false)

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="sendCreate" component={SendCreateScreen} />
      <Stack.Screen name="sentSuccessfully" component={SentSuccessfullyScreen} />
      <Stack.Screen name="sendScanQR" component={SendScanQRScreen} />
      <Stack.Screen name="sendCrossShardRefund" component={SentCrossShardRefundScreen} />
      <Stack.Screen name="sendCrossShardProcessing" component={SendCrossShardProcessingScreen} />
    </Stack.Navigator>
  )
}
