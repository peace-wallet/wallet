import React from "react"

import { createStackNavigator } from "@react-navigation/stack"

import {
  WalletCreateScreen,
  WalletRestoreByMnemonicScreen,
  WalletRestoreByPrivateKeyScreen
} from "../../screens"

export type WalletStackParamList = {
  walletCreate: undefined
  walletRestoreByMnemonic: undefined
  walletRestoreByPrivateKey: undefined
}

const Stack = createStackNavigator<WalletStackParamList>()

export const WalletStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="walletCreate" component={WalletCreateScreen} />
      <Stack.Screen name="walletRestoreByMnemonic" component={WalletRestoreByMnemonicScreen} />
      <Stack.Screen name="walletRestoreByPrivateKey" component={WalletRestoreByPrivateKeyScreen} />
    </Stack.Navigator>
  )
}
