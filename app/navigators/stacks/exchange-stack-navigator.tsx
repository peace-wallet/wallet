import React from "react"

import { createStackNavigator } from "@react-navigation/stack"

import { ExchangeScreen } from "../../screens/exchange/exchange-screen"

export type ExchangeNavigatorParamList = {
  exchange: undefined
}

const Stack = createStackNavigator<ExchangeNavigatorParamList>()

export const ExchangeStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="exchange" component={ExchangeScreen} />
    </Stack.Navigator>
  )
}
