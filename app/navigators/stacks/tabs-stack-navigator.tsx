import React from "react"

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"

import { BottomTabBar } from "../../components/base"
import { BalanceStackNavigator } from "./balance-stack-navigator"
import { ExchangeStackNavigator } from "./exchange-stack-navigator"
import { SettingsStackNavigator } from "./settings-stack-navigator"

export type TabsStackParamList = {
  balance: undefined
  exchange: undefined
  settings: undefined
}

const Tab = createBottomTabNavigator<TabsStackParamList>()

export const TabsStackNavigator = () => {
  return (
    <Tab.Navigator tabBar={(props) => <BottomTabBar {...props} />}>
      <Tab.Screen
        name="balance"
        component={BalanceStackNavigator}
        options={{ tabBarLabel: "tabs.balance" }}
      />
      {/* <Tab.Screen
        name="exchange"
        component={ExchangeStackNavigator}
        options={{ tabBarLabel: "tabs.exchange" }}
      /> */}
      <Tab.Screen
        name="settings"
        component={SettingsStackNavigator}
        options={{ tabBarLabel: "tabs.settings" }}
      />
    </Tab.Navigator>
  )
}
