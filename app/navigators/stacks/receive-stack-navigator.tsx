import React from "react"

import { createStackNavigator } from "@react-navigation/stack"

import { useTabBarVisible } from "../../hooks"
import {
  ReceiveOptionsScreen,
  ReceiveOptionsScreenParams,
  ReceiveQRScreen,
  ReceiveQRScreenParams
} from "../../screens"

export type ReceiveNavigatorParamList = {
  receiveQR: ReceiveQRScreenParams
  receiveOptions: ReceiveOptionsScreenParams
}

const Stack = createStackNavigator<ReceiveNavigatorParamList>()

export const ReceiveStackNavigator = () => {
  useTabBarVisible(false)

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="receiveQR" component={ReceiveQRScreen} />
      <Stack.Screen name="receiveOptions" component={ReceiveOptionsScreen} />
    </Stack.Navigator>
  )
}
