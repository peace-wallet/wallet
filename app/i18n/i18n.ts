import "moment/locale/uk"
import "moment/locale/ru"

import * as Localization from "expo-localization"
import i18n from "i18n-js"
import moment from "moment"

import en from "./locales/en.json"
import ru from "./locales/ru.json"
import uk from "./locales/uk.json"

i18n.fallbacks = true
i18n.translations = { en, ru, uk }

i18n.locale = Localization.locale || "en"
moment.locale(i18n.locale)
