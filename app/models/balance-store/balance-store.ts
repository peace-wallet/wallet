import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"

import { GetBalancesResult } from "../../services/api"
import { CoinsFormatter } from "../../utils/formatters"
import { Balance, BalanceModel } from "../balance/balance"
import { Coin } from "../coin/coin"
import { withEnvironment, withStatus } from "../extensions"
import { StoreError, withError } from "../extensions/with-error"

export const BalanceStoreModel = types
  .model("BalanceStore")
  .props({
    balances: types.array(BalanceModel),
  })
  .extend(withError)
  .extend(withStatus)
  .extend(withEnvironment)
  .views((self) => ({
    availableBalancesByCoin: (
      coin: Coin,
      options: { shardID?: number; excludeEmpty?: boolean; format?: boolean } = {},
    ): Balance[] => {
      const maxInt32Value = Math.pow(2, 32) - 1
      const { shardID = undefined, excludeEmpty = false, format = true } = options

      let balances = []

      if (shardID) {
        balances = self.balances.filter((balance) => balance.shardID === shardID)
      } else {
        if (coin.name === "btc") {
          balances = self.balances.filter((balance) => balance.shardID === coin.shardID)
        }

        if (coin.name === "jax") {
          balances = self.balances.filter(
            (balance) => balance.shardID !== 0 && balance.shardID !== maxInt32Value,
          )
        }

        if (coin.name === "jxn") {
          balances = self.balances.filter((balance) => balance.shardID === coin.shardID)
        }
      }

      if (excludeEmpty) {
        balances = balances.filter((balanceByShard) => balanceByShard.availableBalance > 0)
      }

      return balances
        .map((balanceByShard) => {
          return {
            ...balanceByShard,
            availableBalance: format
              ? CoinsFormatter.toBiggestUnits(coin.name, balanceByShard.availableBalance)
              : balanceByShard.availableBalance,
          }
        })
        .sort((a, b) => a.shardID - b.shardID)
    },
  }))
  .views((self) => ({
    balanceByCoin: (
      coin: Coin,
      shardID?: number,
    ): {
      available: number
      locked: number
      total: number
    } => {
      const balances = self.availableBalancesByCoin(coin, { shardID, format: false })

      const availableBalance = balances.reduce((a, b) => a + b.availableBalance, 0) ?? 0
      const lockedBalance = balances.reduce((a, b) => a + b.lockedBalance, 0) ?? 0
      const totalBalance = availableBalance + lockedBalance ?? 0

      return {
        available: CoinsFormatter.toBiggestUnits(coin.name, availableBalance),
        locked: CoinsFormatter.toBiggestUnits(coin.name, lockedBalance),
        total: CoinsFormatter.toBiggestUnits(coin.name, totalBalance),
      }
    },
  }))
  .actions((self) => {
    const handleError = (error: StoreError) => {
      self.setStatus("error")
      self.setError(error)
    }

    const saveBalances = (balances: Balance[]) => {
      self.balances.replace(balances)
    }

    const getBalances = flow(function* getBalances(address: string) {
      self.setError(null)
      self.setStatus("pending")

      const result: GetBalancesResult = yield self.environment.api.balance.getBalances(address)

      if (result.kind === "ok") {
        const _balances = []
        result.data.forEach((balance) => {
          _balances.push({
            shardID: balance.shardID,
            availableBalance: balance.balance,
            lockedBalance: balance.balanceLocked,
          })
        })
        saveBalances(_balances)
        self.setStatus("done")
      } else {
        handleError(result)
      }
    })

    const reset = () => {
      self.balances.clear()
    }

    return {
      getBalances,
      reset,
    }
  })

type BalanceStoreType = Instance<typeof BalanceStoreModel>
export interface BalanceStore extends BalanceStoreType {}
type BalanceStoreSnapshotType = SnapshotOut<typeof BalanceStoreModel>
export interface BalanceStoreSnapshot extends BalanceStoreSnapshotType {}
export const createBalanceStoreDefaultModel = () => types.optional(BalanceStoreModel, {})
