import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const LockedUTXOsStatsModel = types.model("LockedUTXOsStats").props({
  totalCount: types.number,
  totalAmount: types.number,
})

type LockedUTXOsStatsType = Instance<typeof LockedUTXOsStatsModel>
export interface LockedUTXOsStats extends LockedUTXOsStatsType {}
type LockedUTXOsStatsSnapshotType = SnapshotOut<typeof LockedUTXOsStatsModel>
export interface LockedUTXOsStatsSnapshot extends LockedUTXOsStatsSnapshotType {}
export const LockedUTXOsStatsDefaultModel = () => types.optional(LockedUTXOsStatsModel, {})
