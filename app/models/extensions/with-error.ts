import { IObservableValue, observable } from "mobx"

export type StoreError = {
  kind: string
  message: string
  source?: {
    code: number
    message: string
  }
}

/**
 * Adds a error field to the model often for tracking api access.
 *
 * This property is a string which can be observed, but will not
 * participate in any serialization.
 *
 * Use this to extend your models:
 *
 * ```ts
 *   types.model("MyModel")
 *     .props({})
 *     .actions(self => ({}))
 *     .extend(withError) // <--- time to shine baby!!!
 * ```
 *
 * This will give you these 3 options:
 *
 *   .error             // returns aa error message or null
 *   .error = "message" // change the error directly
 *   .setError("error") // change the error and trigger an mst action
 */
export const withError = () => {
  /**
   * The observable backing store for the error field.
   */
  const error: IObservableValue<StoreError | null> = observable.box(null)

  return {
    views: {
      // a getter
      get error() {
        return error.get() as StoreError
      },
      // as setter
      set error(value: StoreError) {
        error.set(value)
      },
    },
    actions: {
      /**
       * Set the error to something new.
       *
       * @param value The new status.
       */
      setError(value: StoreError | null) {
        error.set(value)
      },
    },
  }
}
