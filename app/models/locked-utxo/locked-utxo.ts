import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const LockedUTXOModel = types.model("LockedUTXO").props({
  hash: types.string,
  shardID: types.number,
  amount: types.number,
  unlockingBlock: types.number,
  timestamp: types.number,
  unlockingTimestamp: types.number,
  output: types.number,
})

type LockedUTXOType = Instance<typeof LockedUTXOModel>
export interface LockedUTXO extends LockedUTXOType {}
type LockedUTXOSnapshotType = SnapshotOut<typeof LockedUTXOModel>
export interface LockedUTXOSnapshot extends LockedUTXOSnapshotType {}
export const createLockedUTXODefaultModel = () => types.optional(LockedUTXOModel, {})
