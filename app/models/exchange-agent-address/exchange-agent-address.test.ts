import { ExchangeAgentAddressModel } from "./exchange-agent-address"

test("can be created", () => {
  const instance = ExchangeAgentAddressModel.create({
    ip: "159.89.198.46",
    port: 4000,
  })

  expect(instance).toBeTruthy()
})
