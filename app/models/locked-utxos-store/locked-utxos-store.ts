import { LockedUTXOsStatsModel } from "./../locked-utxos-stats/locked-utxos-stats"
import { LockedUTXO, LockedUTXOModel } from "./../locked-utxo/locked-utxo"
import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"

import { withEnvironment, withStatus } from "../extensions"
import { StoreError, withError } from "../extensions/with-error"
import moment from "moment"
import { GetLockedUTXOsResult } from "../../services/api"

export const LockedUTXOsStoreModel = types
  .model("LockedUTXOsStore")
  .props({
    lockedUTXOs: types.array(LockedUTXOModel),
    nearestPreviousDate: types.maybeNull(types.string),
    loadedAllPreviousItems: types.optional(types.boolean, false),
    stats: types.maybeNull(LockedUTXOsStatsModel),
    from: types.maybeNull(types.string),
    to: types.maybeNull(types.string),
  })
  .extend(withError)
  .extend(withStatus)
  .extend(withEnvironment)
  .views((self) => ({
    lockedUTXOsByDate: (shardIDs: number[]) => {
      const sections: { title: string; items: LockedUTXO[] }[] = []

      shardIDs.forEach((shardID) => {
        const items = self.lockedUTXOs.filter((utxo) => utxo.shardID === shardID)

        items.forEach((item) => {
          const date = moment.unix(item.timestamp).format("YYYY-MM-DD")

          const section = sections.find((_section) => _section.title === date)
          if (section) {
            section.items.push(item)
            section.items.sort((a, b) => (moment(a.timestamp) > moment(b.timestamp) ? 1 : -1))
          } else {
            sections.push({ title: date, items: [item] })
          }

          sections.sort((a, b) => (moment(a.title) < moment(b.title) ? 1 : -1))
        })
      })

      return sections
    },
  }))
  .actions((self) => {
    const handleError = (error: StoreError) => {
      self.setStatus("error")
      self.setError(error)
    }

    const addOrUpdate = (item: LockedUTXO) => {
      if (!self.lockedUTXOs.includes(item)) {
        self.lockedUTXOs.push(item)
        return
      }

      const index = self.lockedUTXOs.findIndex(
        (_item) => _item.hash === item.hash && _item.output === item.output,
      )
      self.lockedUTXOs[index] = item
    }

    const getLockedUTXOs = flow(function* getLockedUTXOs(
      address: string,
      shardIDS: number[],
      dayClosed = false,
      refresh = true,
      offset = 0,
      limit = 50,
    ) {
      self.setError(null)
      self.setStatus("pending")

      let from = null
      let to = null

      if (refresh) {
        from = moment().set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).format()
        to = moment().set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).add(1, "day").format()
        self.loadedAllPreviousItems = false
      } else {
        shardIDS.forEach((shardID) =>
          self.lockedUTXOs.forEach((item) => {
            if (!self.nearestPreviousDate) {
              self.nearestPreviousDate = moment(item.timestamp).format()
            } else if (moment(item.timestamp).isBefore(moment(self.nearestPreviousDate))) {
              // self.nearestPreviousDate = item.timestamp
            }
          }),
        )

        from = !dayClosed
          ? self.from
          : moment(self.nearestPreviousDate)
              .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
              .format()
        to = !dayClosed
          ? self.to
          : moment(self.nearestPreviousDate)
              .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
              .add(1, "day")
              .format()
      }

      if (!moment(from).isValid() && !moment(to).isValid()) {
        return null
      }

      self.from = from
      self.to = to

      const result: GetLockedUTXOsResult = yield self.environment.api.lockedUTXOs.getLockedUTXOs(
        address,
        shardIDS,
        moment(from).unix(),
        moment(to).unix(),
        offset,
        limit,
      )

      if (result.kind === "ok") {
        const nearestPreviousDate = result.data.nearestDates.previous

        if (nearestPreviousDate) {
          self.nearestPreviousDate = nearestPreviousDate
        } else {
          self.loadedAllPreviousItems = true
        }

        self.stats = result.data.stats

        result.data.utxos.forEach((lockedUTXO) => {
          const _lockedUTXO = {
            hash: lockedUTXO.hash,
            shardID: lockedUTXO.shardId,
            amount: lockedUTXO.amount,
            unlockingBlock: lockedUTXO.unlockingBlock,
            timestamp: lockedUTXO.timestamp,
            unlockingTimestamp: lockedUTXO.unlockingTimestamp,
            output: lockedUTXO.output,
          }
          if (
            self.lockedUTXOs.find(
              (u) => u.hash === lockedUTXO.hash && u.output === lockedUTXO.output,
            )
          ) {
            // console.log("find same utxo", lockedUTXO.hash)
          } else {
            addOrUpdate(_lockedUTXO)
          }
        })
        self.setStatus("done")
      } else {
        handleError(result)
      }

      return result
    })

    const reset = () => {
      self.lockedUTXOs.clear()
      self.from = null
      self.to = null
      self.stats = null
      self.nearestPreviousDate = null
      self.loadedAllPreviousItems = false
    }

    return {
      getLockedUTXOs,
      reset,
    }
  })

type LockedUTXOsStoreType = Instance<typeof LockedUTXOsStoreModel>
export interface LockedUTXOsStore extends LockedUTXOsStoreType {}
type LockedUTXOsStoreSnapshotType = SnapshotOut<typeof LockedUTXOsStoreModel>
export interface LockedUTXOsStoreSnapshot extends LockedUTXOsStoreSnapshotType {}
export const createLockedUTXOsStoreDefaultModel = () => types.optional(LockedUTXOsStoreModel, {})
