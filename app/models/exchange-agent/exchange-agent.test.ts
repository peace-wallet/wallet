import { ExchangeAgentModel } from "./exchange-agent"

test("can be created", () => {
  const instance = ExchangeAgentModel.create({
    id: 123,
    publicKey:
      "041fff05a00690de368e805b73c96669b1dfc758c31cf2ecdc613fd7835b59c66fb8709199146c5978451bc704cd2a036d938606b531fde3a1314e39b6fa8796be",
  })

  expect(instance).toBeTruthy()
})
