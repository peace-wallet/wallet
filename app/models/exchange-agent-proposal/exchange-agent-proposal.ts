import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const ExchangeAgentProposalModel = types
  .model("ExchangeAgentProposal")
  .props({
    exchangeAgentId: types.identifier,
    expires: types.string,
    reservedAmount: types.maybeNull(types.number),
    feeFixedAmount: types.number,
    feePercents: types.number,
    signature: types.string,
  })
  .views((self) => ({
    calculateFee: (amount: number) => {
      return (amount / 100) * self.feePercents + self.feeFixedAmount
    },
  }))

type ExchangeAgentProposalType = Instance<typeof ExchangeAgentProposalModel>
export interface ExchangeAgentProposal extends ExchangeAgentProposalType {}
type ExchangeAgentProposalSnapshotType = SnapshotOut<typeof ExchangeAgentProposalModel>
export interface ExchangeAgentProposalSnapshot extends ExchangeAgentProposalSnapshotType {}
export const createExchangeAgentProposalDefaultModel = () =>
  types.optional(ExchangeAgentProposalModel, {})
