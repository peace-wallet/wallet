import { FeeModel } from "./fee"

test("can be created", () => {
  const instance = FeeModel.create({
    name: "slow",
    title: "Economic",
    value: 1752,
    transactionTimeMs: 7200000,
  })

  expect(instance).toBeTruthy()
})
