import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const FeeModel = types.model("Fee").props({
  name: types.identifier,
  transactionTimeMs: types.maybeNull(types.number),
})

type FeeType = Instance<typeof FeeModel>
export interface Fee extends FeeType {}
type FeeSnapshotType = SnapshotOut<typeof FeeModel>
export interface FeeSnapshot extends FeeSnapshotType {}
export const createFeeDefaultModel = () => types.optional(FeeModel, {})
