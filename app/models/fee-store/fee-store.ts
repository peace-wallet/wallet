import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { withEnvironment } from "../extensions/with-environment"
import { Fee, FeeModel } from "../fee/fee"

export const FeeStoreModel = types
  .model("FeeStore")
  .props({
    fees: types.array(FeeModel),
    selectedFee: types.reference(FeeModel),
  })
  .extend(withEnvironment)
  .actions((self) => {
    const selectFee = (fee: Fee) => {
      self.selectedFee = fee
    }

    return {
      selectFee,
    }
  })

type FeeStoreType = Instance<typeof FeeStoreModel>
export interface FeeStore extends FeeStoreType {}
type FeeStoreSnapshotType = SnapshotOut<typeof FeeStoreModel>
export interface FeeStoreSnapshot extends FeeStoreSnapshotType {}
export const createFeeStoreDefaultModel = () =>
  types.optional(FeeStoreModel, {
    fees: [
      {
        name: "slow",
        transactionTimeMs: null,
      },
      {
        name: "moderate",
        transactionTimeMs: null,
      },
      {
        name: "fast",
        transactionTimeMs: null,
      },
    ],
    selectedFee: "moderate",
  })
