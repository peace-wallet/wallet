import * as bitcoin from "bitcoinjs-lib"
import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"

import { StoreError, withEnvironment, withError, withRootStore, withStatus } from "../extensions"
import { UTXOFee, UTXOFeeModel } from "../utxo-fee/utxo-fee"
import { UTXO, UTXOModel } from "../utxo/utxo"

export const TransactionStoreModel = types
  .model("TransactionStore")
  .props({
    utxos: types.array(UTXOModel),
    fees: types.array(UTXOFeeModel),
    code: types.maybeNull(types.number),
  })
  .extend(withError)
  .extend(withStatus)
  .extend(withRootStore)
  .extend(withEnvironment)
  .views((self) => ({
    feeByName: (feeName: string): UTXOFee => {
      return self.fees.find((fee) => fee.name === feeName)
    },
  }))
  .actions((self) => {
    const handleError = (error: StoreError) => {
      self.setStatus("error")
      self.setError(error)
    }

    const reset = () => {
      self.utxos.clear()
      self.fees.clear()
    }

    const saveUTXOs = (utxos: UTXO[]) => self.utxos.replace(utxos)

    const saveFees = (fees: UTXOFee[]) => self.fees.replace(fees)

    const clearFees = () => self.fees.clear()

    const getUTXOsAndFees = flow(function* getUTXOsAndFees(
      shardID: number,
      address: string,
      amount: number,
    ) {
      self.setError(null)

      if (!address || amount === null || amount === undefined) return

      const _fees = []

      const utxosAndFeesResult = yield self.environment.api.transaction.getUTXOsAndFees(
        shardID,
        address,
        amount,
      )

      if (utxosAndFeesResult.kind === "ok") {
        const { utxos, fees, code } = utxosAndFeesResult.data

        self.code = code

        Object.keys(fees).forEach((fee) => {
          _fees.push({
            name: fee,
            value: fees[fee].fee,
            maxAmount: fees[fee].maxAmount,
          })
        })

        saveUTXOs(utxos)
        saveFees(_fees)
        self.setStatus("done")
      } else {
        handleError(utxosAndFeesResult)
      }
    })

    const fillUTXOsRaw = flow(function* fillUTXOsRaw(shardID: number, hashes: string[]) {
      try {
        const result = yield self.environment.api.transaction.getRaws(shardID, hashes)

        if (result.kind === "ok") {
          const utxos = self.utxos.map((utxo) => {
            return {
              ...utxo,
              raw: result.data[utxo.hash] || utxo.raw,
            }
          })
          saveUTXOs(utxos)
          return [utxos, null]
        } else {
          handleError(result)
          return [null, result]
        }
      } catch (error) {
        handleError({ message: error })
        return [null, error]
      }
    })

    const fillUTXOsRawChunks = flow(function* fillUTXOsRawChunks(shardID: number) {
      const chunkSize = 64

      const chunks = []

      for (let i = 0; i < self.utxos.length; i += chunkSize) {
        const chunk = self.utxos.slice(i, i + chunkSize).map((utxo) => utxo.hash)
        chunks.push(chunk)
      }

      try {
        yield Promise.all(chunks.map((chunk) => fillUTXOsRaw(shardID, chunk)))

        return [self.utxos, null]
      } catch (error) {
        handleError({ message: error })
        return [null, error]
      }
    })

    const createRegularTransaction = flow(function* createRegularTransaction(
      shardID: number,
      receiverAddress: string,
      amount: number,
      fee: number,
    ): Generator<Promise<any>, any, any> {
      self.setError(null)
      self.setStatus("pending")

      const { network, walletStore } = self.rootStore
      const { node: senderNode, address: senderAddress } = walletStore

      const [, fillUTXOsError] = yield fillUTXOsRawChunks(shardID)

      if (fillUTXOsError) return [null, fillUTXOsError]

      try {
        /* --------------------------- Create transaction --------------------------- */

        const psbt = new bitcoin.Psbt({ network })

        let utxosAmount = 0

        self.utxos.forEach((utxo) => {
          utxosAmount += utxo.amount

          psbt.addInput({
            hash: utxo.hash,
            index: utxo.output,
            nonWitnessUtxo: Buffer.from(utxo.raw, "hex"),
          })
        })

        // Sender change
        if (utxosAmount - amount - fee > 0) {
          psbt.addOutput({
            value: utxosAmount - amount - fee,
            address: senderAddress,
          })
        }

        // Receiver output
        psbt.addOutput({
          value: amount,
          address: receiverAddress,
        })

        psbt.signAllInputs(senderNode)
        psbt.validateSignaturesOfAllInputs()
        psbt.finalizeAllInputs()

        const hex = psbt.extractTransaction().toHex()

        const [hash, error] = yield publish(shardID, hex)

        if (hash) {
          self.setStatus("done")
          return [
            {
              hash,
              hex,
            },
            null,
          ]
        } else {
          handleError(error)
          return [null, error]
        }
      } catch (error) {
        handleError({ message: error })
        return [null, error]
      }
    })

    const prepareRegularTransaction = flow(function* prepareRegularTransaction(
      shardID: number,
      receiverAddress: string,
      amount: number,
      fee: number,
    ) {
      const { network, walletStore } = self.rootStore
      const { node: senderNode, address: senderAddress } = walletStore

      const [, fillUTXOsError] = yield fillUTXOsRawChunks(shardID)

      if (fillUTXOsError) return [null, fillUTXOsError]

      try {
        /* --------------------------- Create transaction --------------------------- */

        const psbt = new bitcoin.Psbt({ network })

        let utxosAmount = 0

        self.utxos.forEach((utxo) => {
          utxosAmount += utxo.amount

          psbt.addInput({
            hash: utxo.hash,
            index: utxo.output,
            nonWitnessUtxo: Buffer.from(utxo.raw, "hex"),
          })
        })

        // Sender change
        if (utxosAmount - amount - fee > 0) {
          psbt.addOutput({
            value: utxosAmount - amount - fee,
            address: senderAddress,
          })
        }

        // Receiver output
        psbt.addOutput({
          value: amount,
          address: receiverAddress,
        })

        psbt.signAllInputs(senderNode)
        psbt.validateSignaturesOfAllInputs()
        psbt.finalizeAllInputs()

        const hex = psbt.extractTransaction().toHex()
        const hash = psbt.extractTransaction().getHash().reverse().toString("hex")

        return [
          {
            hash,
            hex,
          },
          null,
        ]
      } catch (error) {
        handleError({ message: error })
        return [null, error]
      }
    })

    const createCrossShardTransactionExchangeInit = async (
      receiverAddress: string,
      sourceShardID: number,
      destinationShardID: number,
      amount: number,
      exchangeAgentFee: number,
      exchangeAgentUrl: string,
    ): Promise<
      [
        {
          exchangeTxUUID: string
          exchangeTxSourceShardLock: number
          exchangeTxDestinationShardLock: number
        } | null,
        StoreError | null,
      ]
    > => {
      console.log("CSTX Start")

      const senderNode = self.rootStore.walletStore.node
      const senderNodeUncompressed = bitcoin.ECPair.fromPrivateKey(senderNode.privateKey, {
        compressed: false,
      })

      const senderPubKeyHex = senderNodeUncompressed.publicKey.toString("hex")

      try {
        const initExchangeResult = await self.environment.api.exchangeAgent.exchangeInit(
          exchangeAgentUrl,
          0,
          sourceShardID,
          destinationShardID,
          amount,
          amount - exchangeAgentFee,
          receiverAddress,
          senderPubKeyHex,
        )

        if (initExchangeResult.kind === "ok") {
          if (initExchangeResult.data.code !== 0) {
            console.log("initExchangeResult", initExchangeResult)
            return [null, { message: "Init failed. See EA response for details." }]
          } else {
            const {
              uuid: exchangeTxUUID,
              sourceShardMultiSigLockWindowBlocks: exchangeTxSourceShardLock,
              destinationShardMultiSigLockWindowBlocks: exchangeTxDestinationShardLock,
            } = initExchangeResult.data

            console.log("CSTX init. success:", initExchangeResult.data)
            console.log("Source shard lock time window:", exchangeTxSourceShardLock, "blocks")
            console.log(
              "Destination shard lock time window:",
              exchangeTxDestinationShardLock,
              "blocks",
            )

            return [
              {
                exchangeTxUUID,
                exchangeTxSourceShardLock,
                exchangeTxDestinationShardLock,
              },
              null,
            ]
          }
        } else {
          console.log("Init failed.", initExchangeResult)
          return [null, initExchangeResult]
        }
      } catch (error) {
        console.log("Init failed.", error)
        return [null, { message: `Init failed. ${error}` }]
      }
    }

    const createCrossShardTransactionMultiSigPublish = async (
      sourceShardID: number,
      amount: number,
      fee: number,
      exchangeAgentPublicKey: string,
      exchangeTXSourceShardLock: number,
      exchangeTXDestinationShardLock: number,
    ): Promise<
      [
        {
          refundTransactionHex: string
          sourceShardFundsLockingTxHash: string
          sourceShardMultiSigRedeemHex: string
          destinationShardMultiSigRedeemHex: string
        } | null,
        StoreError | null,
      ]
    > => {
      const { network, walletStore } = self.rootStore
      const { node: senderNode, address: senderAddress } = walletStore

      /* ---------------------- Sender --------------------- */
      const senderNodeUncompressed = bitcoin.ECPair.fromPrivateKey(senderNode.privateKey, {
        compressed: false,
      })

      console.log("Sender PKey", senderNode.privateKey.toString("hex"))
      console.log("Sender PubKey", senderNodeUncompressed.publicKey.toString("hex"))

      /* ---------------------- Receiver --------------------- */

      const eadPKeyBinary = Buffer.from(exchangeAgentPublicKey, "hex")
      const eadNode = bitcoin.ECPair.fromPublicKey(eadPKeyBinary, {
        compressed: false,
      })

      const eadPubKey = eadNode.publicKey
      console.log("EAD PubKey: ", eadPubKey.toString("hex"))

      const multiSigParticipantsCount = 2

      const sourceShardRefundPubKey = senderNodeUncompressed.publicKey.toString("hex")
      console.log("Source shard refund address pub key (sender): ", sourceShardRefundPubKey)

      const destinationShardRefundPubKey = eadNode.publicKey.toString("hex")
      console.log("Destination shard refund address pub key (ead): ", destinationShardRefundPubKey)

      try {
        const sourceShardMultiSig = bitcoin.payments.p2sh({
          redeem: bitcoin.payments.p2mstl({
            m: multiSigParticipantsCount,
            n: multiSigParticipantsCount,
            refundDeferringPeriod: exchangeTXSourceShardLock,
            refund: sourceShardRefundPubKey,
            pubkeys: [senderNodeUncompressed.publicKey, eadNode.publicKey],
            network,
            sortKeys: true,
          }),
        })

        if (
          sourceShardMultiSig.redeem === undefined ||
          sourceShardMultiSig.redeem.output === undefined
        ) {
          return [null, { message: "Invalid multi sig." }]
        }
        console.log("Source shard multi sig.:", sourceShardMultiSig)
        console.log(
          "Source shard multi sig. redeem:",
          sourceShardMultiSig.redeem.output.toString("hex"),
        )

        const destinationShardMultiSig = bitcoin.payments.p2sh({
          redeem: bitcoin.payments.p2mstl({
            m: multiSigParticipantsCount,
            n: multiSigParticipantsCount,
            refundDeferringPeriod: exchangeTXDestinationShardLock,
            refund: destinationShardRefundPubKey,
            pubkeys: [senderNodeUncompressed.publicKey, eadNode.publicKey],
            network,
            sortKeys: true,
          }),
        })

        if (
          destinationShardMultiSig.redeem === undefined ||
          destinationShardMultiSig.redeem.output === undefined
        ) {
          return [null, { message: "Invalid multi sig." }]
        }
        console.log("Destination shard multi sig.:", destinationShardMultiSig)
        console.log(
          "Destination shard multi sig. redeem:",
          destinationShardMultiSig.redeem.output.toString("hex"),
        )

        const [
          preparedFundsLockingTransactionData,
          preparedFundsLockingTransactionError,
        ] = await prepareRegularTransaction(sourceShardID, sourceShardMultiSig.address, amount, fee)

        if (preparedFundsLockingTransactionError)
          return [null, preparedFundsLockingTransactionError]

        const {
          hash: fundsLockingTransactionHash,
          hex: fundsLockingTransactionHex,
        } = preparedFundsLockingTransactionData

        console.log("fundsLockingTransactionHash", fundsLockingTransactionHash)
        console.log("fundsLockingTransactionHex", fundsLockingTransactionHex)
        console.log("sourceShardMultiSig.redeem.refund", sourceShardMultiSig.redeem.refund)

        /* ------------------------ Create refund transaction ----------------------- */
        const psbt = new bitcoin.Psbt({ network })

        const utxoTx = bitcoin.Transaction.fromHex(fundsLockingTransactionHex)

        psbt.addInput({
          hash: fundsLockingTransactionHash,
          index: utxoTx.outs.findIndex((out) => out.value === amount) ?? 1,
          redeemScript: sourceShardMultiSig.redeem.output,
          nonWitnessUtxo: Buffer.from(fundsLockingTransactionHex, "hex"),
        })

        psbt.addOutput({
          value: amount - fee,
          address: senderAddress,
        })

        psbt.setVersion(1)

        psbt.signAllInputs(senderNodeUncompressed)
        psbt.validateSignaturesOfAllInputs()
        psbt.finalizeAllInputs()

        const refundTransactionHex = psbt.extractTransaction().toHex()

        console.log("Refund hex", refundTransactionHex)

        /* -------------- Publish funds lock transaction to the network ------------- */
        const [publishedTransactionHash, publishError] = await publish(
          sourceShardID,
          fundsLockingTransactionHex,
        )

        if (publishError) return [null, publishError]

        console.log("publishedTransactionHash", publishedTransactionHash)

        console.log("Source shard funds locking TX hash", publishedTransactionHash)
        console.log("Source shard funds locking TX hex", fundsLockingTransactionHex)

        return [
          {
            refundTransactionHex,
            sourceShardFundsLockingTxHash: publishedTransactionHash,
            sourceShardMultiSigRedeemHex: sourceShardMultiSig.redeem.output.toString("hex"),
            destinationShardMultiSigRedeemHex: destinationShardMultiSig.redeem.output.toString(
              "hex",
            ),
          },
          null,
        ]
      } catch (error) {
        return [null, { message: error }]
      }
    }

    const createCrossShardTransactionExchangeLock = async (
      sourceShardMultiSigRedeemHex: string,
      sourceShardFundsLockingTxHash: string,
      exchangeTxUUID: string,
      exchangeAgentUrl: string,
    ): Promise<[any | null, StoreError | null]> => {
      try {
        const result = await self.environment.api.exchangeAgent.exchangeLock(
          exchangeAgentUrl,
          exchangeTxUUID,
          sourceShardMultiSigRedeemHex,
          sourceShardFundsLockingTxHash,
        )

        console.log("Exchange Agent lock result", result)

        if (result.kind === "ok") {
          if (result.data.code !== 0) {
            return [null, { message: "Lock failed" }]
          } else {
            console.log("Lock success", result.data)
            return [result.data, null]
          }
        } else {
          return [null, result]
        }
      } catch (e) {
        return [null, { message: "Lock failed. See EA response for details." }]
      }
    }

    const createCrossShardTransactionSignAndPublish = async (
      receiverAddress: string,
      amount: number,
      sourceShardID: number,
      sourceShardMultiSigRedeemHex: string,
      sourceShardFundsLockingTxHash: string,
      destinationShardID: number,
      destinationShardMultiSigRedeemHex: string,
      exchangeAgentFee: number,
      exchangeAgentPublicKey: string,
      rawCSTXHex: string,
      destinationShardFundsLockTxHash: string,
    ): Promise<[{ hash: string; hex: string } | null, StoreError | null]> => {
      const { network, walletStore } = self.rootStore
      const { node } = walletStore

      /* ---------------------- Sender --------------------- */
      const senderNode = bitcoin.ECPair.fromPrivateKey(node.privateKey, {
        compressed: false,
      })
      const senderPubKey = senderNode.publicKey

      /* ---------------------- Receiver --------------------- */
      const eadPKeyBinary = Buffer.from(exchangeAgentPublicKey, "hex")
      const eadNode = bitcoin.ECPair.fromPublicKey(eadPKeyBinary, {
        compressed: false,
      })

      const eadPubKey = eadNode.publicKey
      console.log("EAD PubKey: ", eadPubKey.toString("hex"))

      const eadAddress = bitcoin.payments.p2pkh({
        pubkey: eadNode.publicKey,
        network,
      }).address

      console.log("Destination shard funds locking TX hash", destinationShardFundsLockTxHash)

      console.log("x-x-x-x-x-x-x")
      console.log("CSTX", rawCSTXHex)
      console.log("sourceShardID", sourceShardID)
      console.log("destinationShardID", destinationShardID)
      console.log("sourceShardFundsLockingTxHash", sourceShardFundsLockingTxHash)
      console.log("destinationShardFundsLockTXHash", destinationShardFundsLockTxHash)
      console.log("eadPubKey", eadPubKey.toString("hex"))
      console.log("senderPubKey", senderPubKey.toString("hex"))
      console.log("sourceAmount", amount)
      console.log("destinationAmount", amount - exchangeAgentFee)
      console.log("sourceShardMultiSigRedeemHex", sourceShardMultiSigRedeemHex)
      console.log("destinationShardMultiSigRedeemHex", destinationShardMultiSigRedeemHex)
      console.log("x-x-x-x-x-x-x")

      const cstx = bitcoin.Transaction.fromHex(rawCSTXHex)

      const inputsAndErr = bitcoin.validateRaw2SigCSTX(
        cstx,
        sourceShardFundsLockingTxHash,
        destinationShardFundsLockTxHash,
        eadPubKey,
        eadAddress,
        receiverAddress,
        amount,
        amount - exchangeAgentFee,
        Buffer.from(sourceShardMultiSigRedeemHex, "hex"),
        Buffer.from(destinationShardMultiSigRedeemHex, "hex"),
        network,
      )

      const validationErr = inputsAndErr[6]
      if (validationErr == null) {
        console.log("Validation successfully passed")
      } else {
        return [null, { message: "Validation failed" }]
      }

      /* ---------------------------- Sign transaction ---------------------------- */

      const [signedTxHex, signedTxError] = await signCSTX(
        senderNode,
        inputsAndErr,
        cstx,
        sourceShardID,
        destinationShardID,
        sourceShardMultiSigRedeemHex,
        destinationShardMultiSigRedeemHex,
      )
      if (signedTxError) {
        return [null, signedTxError]
      }

      console.log("signedTxHex", signedTxHex)

      /* ------------------------- Publish to source shard ------------------------ */

      const [, sourceShardError] = await publish(sourceShardID, signedTxHex)

      if (sourceShardError) return [null, sourceShardError]

      /* ---------------------- Publish to destination shard ---------------------- */

      const [destinationShardHash, destinationShardError] = await publish(
        destinationShardID,
        signedTxHex,
      )

      if (destinationShardError) return [null, destinationShardError]

      console.log("destinationShardHash", destinationShardHash)

      return [
        {
          hash: destinationShardHash,
          hex: signedTxHex,
        },
        null,
      ]
    }

    const checkTransactionConfirmations = async (
      shardID: number,
      txHash: string,
    ): Promise<{ confirmations: number }> => {
      console.log("checking lock TX to be mined...")
      console.log("shardID", shardID)
      console.log("txHash", txHash)

      // TODO: Change to actual network
      const response = await self.environment.api.apisauce.axiosInstance.get(
        `https://testnet.explore.jax.net/api/v1/shards/${shardID}/transactions/${txHash}/`,
      )

      const {
        data: { confirmations = 0 },
      } = response.data

      console.log(`Confirmations: ${confirmations}`)

      return { confirmations }
    }

    const checkTransactionProcessedByEAD = async (
      exchangeAgentUrl: string,
      exchangeTxUUID: string,
    ): Promise<
      [
        {
          code: number
          confirmationsRequired: number
          confirmations: number
          rawCSTXHex: string
          destinationShardFundsLockTxHash: string
          destinationShardFundsLockTxOutIndex: number
        } | null,
        StoreError | null,
      ]
    > => {
      console.log("checking lock TX is processed by the EAD...")

      console.log("exchangeAgentUrl", exchangeAgentUrl)
      console.log("exchangeTxUUID", exchangeTxUUID)

      const response = await self.environment.api.exchangeAgent.checkTransactionProcessed(
        exchangeAgentUrl,
        exchangeTxUUID,
      )

      if (response.kind === "ok") {
        // todo: перевірити підпис агента у відповіді.
        console.log("response:", response.data)

        const { code } = response.data

        if (code === 0 || code === 2) {
          const {
            crossShardTx,
            lockTxHash,
            lockTxOutNum,
            sourceShardLockTxConfirmationsCount,
            destinationShardLockTxConfirmationsCount,
            sourceShardLockTxConfirmationsRequired,
            destinationShardLockTxConfirmationsRequired,
          } = response.data

          return [
            {
              code,
              confirmationsRequired:
                sourceShardLockTxConfirmationsRequired +
                destinationShardLockTxConfirmationsRequired,
              confirmations:
                sourceShardLockTxConfirmationsCount + destinationShardLockTxConfirmationsCount,
              rawCSTXHex: crossShardTx,
              destinationShardFundsLockTxHash: lockTxHash,
              destinationShardFundsLockTxOutIndex: lockTxOutNum,
            },
            null,
          ]
        } else if (code === 255 || code === 1) {
          return [null, { message: "CSTX failed by the EAD" }]
        } else {
          return [null, null]
        }
      } else {
        return [null, response]
      }
    }

    const checkInputTransaction = async (
      destinationShardID: number,
      address: string,
      transactionHash: string,
      transactionInputIndex: number,
    ): Promise<
      [
        {
          isUsed: boolean
        } | null,
        StoreError | null,
      ]
    > => {
      const response = await self.environment.api.transaction.checkInputTransaction(
        destinationShardID,
        address,
        transactionHash,
        transactionInputIndex,
      )

      if (response.kind === "ok") {
        console.log("checkInputTransaction: isUsed: ", response.data)

        const { isUsed } = response.data

        return [
          {
            isUsed,
          },
          null,
        ]
      } else {
        return [null, response]
      }
    }

    const signCSTX = async (
      senderNode: bitcoin.ECPairInterface,
      inputsAndErr: [
        number | null,
        number | null,
        number | null,
        number | null,
        number | null,
        number | null,
        Error | null,
      ],
      cstx: bitcoin.Transaction,
      sourceShardID: number,
      destinationShardID: number,
      sourceShardMultiSigRedeemHex: string,
      destinationShardMultiSigRedeemHex: string,
    ): Promise<[string | null, StoreError | null]> => {
      try {
        const sourceShardAmountInputIndex = inputsAndErr[0]
        if (sourceShardAmountInputIndex == null) {
          return [null, Error("Invalid source shard amount input")]
        }
        console.log("sourceShardAmountInputIndex", sourceShardAmountInputIndex)

        const sourceShardFeeInputIndex = inputsAndErr[1]
        if (sourceShardFeeInputIndex == null) {
          return [null, { message: "Invalid source shard fee input" }]
        }
        console.log("sourceShardFeeInputIndex", sourceShardFeeInputIndex)

        const destinationShardAmountInputIndex = inputsAndErr[2]
        if (destinationShardAmountInputIndex == null) {
          return [null, { message: "Invalid destination shard amount input" }]
        }
        console.log("destinationShardAmountInputIndex", destinationShardAmountInputIndex)

        const destinationShardFeeInputIndex = inputsAndErr[3]
        if (destinationShardFeeInputIndex == null) {
          return [null, { message: "Invalid destination shard fee input" }]
        }
        console.log("destinationShardFeeInputIndex", destinationShardFeeInputIndex)

        const sourceShardOutputIndex = inputsAndErr[4]
        if (sourceShardOutputIndex == null) {
          return [null, { message: "Invalid source shard output" }]
        }
        console.log("sourceShardOutputIndex", sourceShardOutputIndex)

        const destinationShardOutputIndex = inputsAndErr[5]
        if (destinationShardOutputIndex == null) {
          return [null, { message: "Destination source shard output" }]
        }
        console.log("destinationShardOutputIndex", destinationShardOutputIndex)

        const sourceShardAmountInput = cstx.ins[sourceShardAmountInputIndex]
        const sourceShardAmountUTXOResponse = await self.environment.api.transaction.getRaws(
          sourceShardID,
          [sourceShardAmountInput.hash.reverse().toString("hex")],
        )
        if (sourceShardAmountUTXOResponse.kind !== "ok") {
          return [null, sourceShardAmountUTXOResponse]
        }
        const sourceShardAmountUTXOHex =
          sourceShardAmountUTXOResponse.data[Object.keys(sourceShardAmountUTXOResponse.data)[0]]
        console.log(
          "sourceShardAmountUTXOHex",
          sourceShardAmountUTXOHex,
          "hash",
          sourceShardAmountInput.hash.reverse().toString("hex"),
        )
        const sourceShardAmountUTXOBinary = Buffer.from(sourceShardAmountUTXOHex, "hex")

        const destinationShardAmountInput = cstx.ins[destinationShardAmountInputIndex]
        const destinationShardAmountUTXOResponse = await self.environment.api.transaction.getRaws(
          destinationShardID,
          [destinationShardAmountInput.hash.reverse().toString("hex")],
        )

        if (destinationShardAmountUTXOResponse.kind !== "ok") {
          return [null, destinationShardAmountUTXOResponse]
        }

        const destinationShardAmountUTXOHex =
          destinationShardAmountUTXOResponse.data[
            Object.keys(destinationShardAmountUTXOResponse.data)[0]
          ]
        console.log(
          "destinationShardAmountUTXOHex",
          destinationShardAmountUTXOHex,
          "hash",
          destinationShardAmountInput.hash.reverse().toString("hex"),
        )
        const destinationShardAmountUTXOBinary = Buffer.from(destinationShardAmountUTXOHex, "hex")

        let i = 0
        for (const input of cstx.ins) {
          console.log(i, input.hash.toString("hex"))
          i++
        }

        // Sign source shard
        const opts1 = {
          tx: cstx,
          parentTx: sourceShardAmountUTXOBinary,
          redeemScript: Buffer.from(sourceShardMultiSigRedeemHex, "hex"),
        }
        const opts1Signed = bitcoin.addSignatureToTx(opts1, sourceShardAmountInputIndex, senderNode)

        // Sign destination shard
        const opts2 = {
          tx: opts1Signed.tx,
          parentTx: destinationShardAmountUTXOBinary,
          redeemScript: Buffer.from(destinationShardMultiSigRedeemHex, "hex"),
        }
        const opts2Signed = bitcoin.addSignatureToTx(
          opts2,
          destinationShardAmountInputIndex,
          senderNode,
        )

        const signedCSTXHex = opts2Signed.tx.toHex()
        return [signedCSTXHex, null]
      } catch (error) {
        return [null, { message: error }]
      }
    }

    const publish = async (
      shardID: number,
      hex: string,
    ): Promise<[string | null, StoreError | null]> => {
      try {
        const result = await self.environment.api.transaction.publish(shardID, hex)
        if (result.kind === "ok") {
          return [result.data, null]
        } else {
          return [null, result]
        }
      } catch (error) {
        return [null, { message: error }]
      }
    }

    return {
      reset,
      publish,
      clearFees,
      getUTXOsAndFees,
      checkTransactionConfirmations,
      checkTransactionProcessedByEAD,
      checkInputTransaction,
      createRegularTransaction,
      createCrossShardTransactionExchangeInit,
      createCrossShardTransactionMultiSigPublish,
      createCrossShardTransactionExchangeLock,
      createCrossShardTransactionSignAndPublish,
    }
  })

type TransactionStoreType = Instance<typeof TransactionStoreModel>
export interface TransactionStore extends TransactionStoreType {}
type TransactionStoreSnapshotType = SnapshotOut<typeof TransactionStoreModel>
export interface TransactionStoreSnapshot extends TransactionStoreSnapshotType {}
export const createTransactionStoreDefaultModel = () => types.optional(TransactionStoreModel, {})
