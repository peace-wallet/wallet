import * as bitcoin from "bitcoinjs-lib"
import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { deleteUserPinCode } from "@haskkor/react-native-pincode"
import { MAINNET_API_URL, TESTNET_API_URL } from "../../config"
import { createBalanceStoreDefaultModel } from "../balance-store/balance-store"
import { createCoinStoreDefaultModel } from "../coin-store/coin-store"
import { createExchangeAgentStoreDefaultModel } from "../exchange-agent-store/exchange-agent-store"
import { withEnvironment } from "../extensions"
import { createFeeStoreDefaultModel } from "../fee-store/fee-store"
import { createHistoryStoreDefaultModel } from "../history-store/history-store"
import { createLockedUTXOsStoreDefaultModel } from "../locked-utxos-store/locked-utxos-store"
import { createTransactionStoreDefaultModel } from "../transaction-store/transaction-store"
import { createWalletStoreDefaultModel } from "../wallet-store/wallet-store"

/**
 * A RootStore model.
 */
export const RootStoreModel = types
  .model("RootStore")
  .props({
    networkName: types.optional(types.enumeration(["testnet", "mainnet"]), "mainnet"),
    pinCodeScreenEnabled: types.optional(types.boolean, true),
    pinCodeScreenIsVisible: types.optional(types.boolean, false),
    //
    feeStore: createFeeStoreDefaultModel(),
    coinStore: createCoinStoreDefaultModel(),
    walletStore: createWalletStoreDefaultModel(),
    balanceStore: createBalanceStoreDefaultModel(),
    historyStore: createHistoryStoreDefaultModel(),
    transactionStore: createTransactionStoreDefaultModel(),
    exchangeAgentStore: createExchangeAgentStoreDefaultModel(),
    lockedUTXOsStore: createLockedUTXOsStoreDefaultModel(),
  })
  .extend(withEnvironment)
  .views((self) => {
    return {
      get network(): bitcoin.Network {
        return bitcoin.networks[self.networkName === "testnet" ? "testnet" : "bitcoin"]
      },
    }
  })
  .actions((self) => {
    const enablePinCodeScreen = () => {
      self.pinCodeScreenEnabled = true
    }

    const disablePinCodeScreen = () => {
      self.pinCodeScreenEnabled = false
    }

    const showPinCodeScreen = () => {
      self.pinCodeScreenIsVisible = true
    }

    const hidePinCodeScreen = () => {
      self.pinCodeScreenIsVisible = false
    }

    const changeNetworkType = flow(function* (network: "testnet" | "mainnet"): any {
      self.networkName = network

      self.historyStore.reset()
      self.balanceStore.reset()

      setAPIBaseUrl()

      yield self.walletStore.refreshAddress()
    })

    const setAPIBaseUrl = () => {
      self.environment.api.apisauce.setBaseURL(
        self.networkName === "mainnet" ? MAINNET_API_URL : TESTNET_API_URL,
      )
    }

    const reset = async () => {
      self.historyStore.reset()
      self.balanceStore.reset()
      self.walletStore.reset()

      deleteUserPinCode()
      enablePinCodeScreen()
      hidePinCodeScreen()
    }

    const afterCreate = () => {
      setAPIBaseUrl()
    }

    return {
      enablePinCodeScreen,
      disablePinCodeScreen,
      showPinCodeScreen,
      hidePinCodeScreen,
      changeNetworkType,
      reset,
      afterCreate,
    }
  })

/**
 * The RootStore instance.
 */
export interface RootStore extends Instance<typeof RootStoreModel> {}

/**
 * The data of a RootStore.
 */
export interface RootStoreSnapshot extends SnapshotOut<typeof RootStoreModel> {}
