import { UTXOFeeModel } from "./utxo-fee"

test("can be created", () => {
  const instance = UTXOFeeModel.create({
    name: "slow",
    value: 1750,
    maxAmount: 10000000,
  })

  expect(instance).toBeTruthy()
})
