import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const UTXOFeeModel = types.model("UTXOFee").props({
  name: types.identifier,
  value: types.number,
  maxAmount: types.number,
})

type UTXOFeeType = Instance<typeof UTXOFeeModel>
export interface UTXOFee extends UTXOFeeType {}
type UTXOFeeSnapshotType = SnapshotOut<typeof UTXOFeeModel>
export interface UTXOFeeSnapshot extends UTXOFeeSnapshotType {}
export const createBalanceByShardDefaultModel = () => types.optional(UTXOFeeModel, {})
