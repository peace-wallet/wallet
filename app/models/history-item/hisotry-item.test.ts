import { HistoryItemModel } from "./history-item"

test("can be created", () => {
  const instance = HistoryItemModel.create({
    transactionHash: "63cdfe9b786862a3208ee476e003e080d08fcea8bc3660bf76a9b376297a4c03",
    sourceAmount: 100000000,
    address: "n4NHLRU5gZNosLEdxCJaRDhy5Mr9JQrSAa",
    direction: "out",
    timestamp: "2021-05-31T15:12:19Z",
  })

  expect(instance).toBeTruthy()
})
