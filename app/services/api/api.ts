import { ApisauceInstance, create } from "apisauce"

import { ApiConfig, DEFAULT_API_CONFIG } from "./api-config"
import { BalanceApi } from "./balance-api"
import { ExchangeAgentApi } from "./exchange-agent.api"
import { HistoryApi } from "./history-api"
import { LockedUTXOsApi } from "./locked-utxos-api"
import { TransactionApi } from "./transaction-api"

/**
 * Manages all requests to the API.
 */
export class Api {
  /**
   * The underlying apisauce instance which performs the requests.
   */
  apisauce: ApisauceInstance

  /**
   * Configurable options.
   */
  config: ApiConfig

  /**
   * Sub modules
   */
  balance: BalanceApi
  history: HistoryApi
  transaction: TransactionApi
  exchangeAgent: ExchangeAgentApi
  lockedUTXOs: LockedUTXOsApi

  /**
   * Creates the api.
   *
   * @param config The configuration to use.
   */
  constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
    this.config = config
  }

  /**
   * Sets up the API.  This will be called during the bootup
   * sequence and will happen before the first React component
   * is mounted.
   *
   * Be as quick as possible in here.
   */
  setup() {
    // construct the apisauce instance
    this.apisauce = create({
      baseURL: this.config.url,
      timeout: this.config.timeout,
      headers: {
        Accept: "application/json",
      },
    })

    this.balance = new BalanceApi(this.apisauce)
    this.history = new HistoryApi(this.apisauce)
    this.transaction = new TransactionApi(this.apisauce)
    this.exchangeAgent = new ExchangeAgentApi(this.apisauce)
    this.lockedUTXOs = new LockedUTXOsApi(this.apisauce)
  }
}
