import { ApiResponse } from "apisauce"

import { translate } from "../../i18n"

export type ApiProblem = {
  kind:
    | "timeout"
    | "cannot-connect"
    | "server"
    | "unauthorized"
    | "forbidden"
    | "not-found"
    | "rejected"
    | "canceled"
    | "unknown"
    | "bad-data"
  temporary?: boolean
  message: string
  source?: {
    code: number
    message: string
  }
}

export const ApiProblems: { [key: string]: ApiProblem } = {
  /**
   * Times up.
   */
  timeout: {
    kind: "timeout",
    temporary: true,
    message: translate("requestErrors.timeout"),
  },
  /**
   * Cannot connect to the server for some reason.
   */
  cannotConnect: {
    kind: "cannot-connect",
    temporary: true,
    message: translate("requestErrors.cannotConnect"),
  },
  /**
   * The server experienced a problem. Any 5xx error.
   */
  serverError: {
    kind: "server",
    message: translate("requestErrors.server"),
    source: null,
  },
  /**
   * We're not allowed because we haven't identified ourself. This is 401.
   */
  unauthorized: {
    kind: "unauthorized",
    message: translate("requestErrors.unauthorized"),
  },
  /**
   * We don't have access to perform that request. This is 403.
   */
  forbidden: {
    kind: "forbidden",
    message: translate("requestErrors.forbidden"),
  },

  /**
   * Unable to find that resource.  This is a 404.
   */
  notFound: {
    kind: "not-found",
    message: translate("requestErrors.notFound"),
  },
  /**
   * All other 4xx series errors.
   */
  rejected: {
    kind: "rejected",
    message: translate("requestErrors.rejected"),
  },
  /**
   * All other 4xx series errors.
   */
  canceled: {
    kind: "canceled",
    message: translate("requestErrors.canceled"),
  },
  /**
   * Something truly unexpected happened. Most likely can try again. This is a catch all.
   */
  unknown: {
    kind: "unknown",
    temporary: true,
    message: translate("requestErrors.unknown"),
  },
  /**
   * The data we received is not in the expected format.
   */
  badData: {
    kind: "bad-data",
    message: translate("requestErrors.badData"),
  },
}

/**
 * Attempts to get a common cause of problems from an api response.
 *
 * @param response The api response.
 */
export function getGeneralApiProblem(response: ApiResponse<any>): ApiProblem | void {
  switch (response.problem) {
    case "CONNECTION_ERROR":
      return ApiProblems.cannotConnect
    case "NETWORK_ERROR":
      return ApiProblems.cannotConnect
    case "TIMEOUT_ERROR":
      return ApiProblems.timeout
    case "SERVER_ERROR":
      return { ...ApiProblems.serverError, source: response.data }
    case "UNKNOWN_ERROR":
      return ApiProblems.unknown
    case "CLIENT_ERROR":
      switch (response.status) {
        case 401:
          return ApiProblems.unauthorized
        case 403:
          return ApiProblems.forbidden
        case 404:
          return ApiProblems.notFound
        default:
          return ApiProblems.rejected
      }
    case "CANCEL_ERROR":
      return ApiProblems.canceled
  }

  return null
}
