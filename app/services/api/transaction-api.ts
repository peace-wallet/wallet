import { ApisauceInstance } from "apisauce"

import { ApiProblems, getGeneralApiProblem } from "./api-problem"
import {
  GetCheckInputTransactionResult,
  GetRawTransactionResult,
  GetUTXOsAndFeesResult,
  PostTransactionResult,
} from "./api.types"

export class TransactionApi {
  private api: ApisauceInstance

  constructor(api: ApisauceInstance) {
    this.api = api
  }

  async getUTXOsAndFees(
    shardID: number,
    address: string,
    amount: number,
  ): Promise<GetUTXOsAndFeesResult> {
    const response: any = await this.api.get(
      `/shards/${shardID}/addresses/${address}/utxos/?amount=${amount}`,
      {},
      { timeout: 60000 },
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const data = response.data.data

    try {
      return { kind: "ok", data }
    } catch (e) {
      __DEV__ && console.tron.log(e.message)
      return ApiProblems.badData
    }
  }

  async getRaws(shardID: number, hashes: string[]): Promise<GetRawTransactionResult> {
    const response: any = await this.api.post(
      `/shards/${shardID}/transactions/`,
      { hashes },
      { timeout: 60000 },
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const data = response.data.data

    try {
      return { kind: "ok", data }
    } catch (e) {
      __DEV__ && console.tron.log(e.message)
      return ApiProblems.badData
    }
  }

  async publish(shardID: number, transaction: string): Promise<PostTransactionResult> {
    const response: any = await this.api.post(
      `/shards/${shardID}/transactions/publish/`,
      {
        transaction,
      },
      { timeout: 60000 },
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const data = response.data.data.hash

    try {
      return { kind: "ok", data }
    } catch (e) {
      __DEV__ && console.tron.log(e.message)
      return ApiProblems.badData
    }
  }

  async checkInputTransaction(
    shardID: number,
    address: string,
    transactionHash: string,
    transactionInputIndex: number,
  ): Promise<GetCheckInputTransactionResult> {
    const response: any = await this.api.get(
      `/shards/${shardID}/addresses/${address}/check-input-tx/?txHash=${transactionHash}&txInputIndex=${transactionInputIndex}`,
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const data = response.data.data

    try {
      return { kind: "ok", data }
    } catch (e) {
      __DEV__ && console.tron.log(e.message)
      return ApiProblems.badData
    }
  }
}
