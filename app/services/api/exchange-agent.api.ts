import { ApisauceInstance } from "apisauce"

import { ApiProblems, getGeneralApiProblem } from "./api-problem"
import {
  ExchangeAgentExchangeInitResult,
  ExchangeAgentExchangeLockResult,
  ExchangeAgentExchangeProcessedResult,
  GetExchangeAgentProposalResult,
  GetExchangeAgentProposalsResult,
  GetExchangeAgentsResult
} from "./api.types"

export class ExchangeAgentApi {
  private api: ApisauceInstance

  constructor(api: ApisauceInstance) {
    this.api = api
  }

  async getExchangeAgents(
    sourceShardID: number,
    destinationShardID: number,
  ): Promise<GetExchangeAgentsResult> {
    const response: any = await this.api.get(
      `/exchange-agents/?sourceShardId=${sourceShardID}&destinationShardId=${destinationShardID}`,
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const data = response.data.data

    try {
      return { kind: "ok", data }
    } catch (e) {
      return ApiProblems.badData
    }
  }

  async getExchangeAgentProposals(
    url: string,
    sourceShardID: number,
    destinationShardID: number,
  ): Promise<GetExchangeAgentProposalsResult> {
    const response: any = await this.api.get(
      `/proposals/?sourceShardId=${sourceShardID}&destinationShardId=${destinationShardID}`,
      {},
      { baseURL: `https://${url}/api/v1` },
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const data = response.data.data

    try {
      return { kind: "ok", data }
    } catch (e) {
      return ApiProblems.badData
    }
  }

  async getExchangeAgentProposal(
    url: string,
    sourceShardID: number,
    destinationShardID: number,
  ): Promise<GetExchangeAgentProposalResult> {
    const response: any = await this.api.get(
      `/proposal/?sourceShardId=${sourceShardID}&destinationShardId=${destinationShardID}`,
      {},
      { baseURL: `https://${url}/api/v1` },
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const data = response.data.data

    try {
      return { kind: "ok", data }
    } catch (e) {
      return ApiProblems.badData
    }
  }

  async exchangeInit(
    url: string,
    type: number,
    sourceShardID: number,
    destinationShardID: number,
    sourceAmount: number,
    destinationAmount: number,
    destinationShardAddress: string,
    senderPubKeyHex: string,
  ): Promise<ExchangeAgentExchangeInitResult> {
    const response: any = await this.api.post(
      `/exchange/init/`,
      {
        type,
        sourceShardId: sourceShardID,
        destinationShardId: destinationShardID,
        sourceAmount,
        destinationAmount,
        destinationShardAddress,
        senderPubKey: senderPubKeyHex,
      },
      { baseURL: `https://${url}/api/v1` },
    )

    console.log("resp", response)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const data = response.data.data

    try {
      return { kind: "ok", data }
    } catch (e) {
      return ApiProblems.badData
    }
  }

  async exchangeLock(
    url: string,
    uuid: string,
    multiSigRedeemScript: string,
    txHash: string,
  ): Promise<ExchangeAgentExchangeLockResult> {
    const response: any = await this.api.post(
      `/exchange/lock/`,
      {
        uuid,
        multiSigRedeemScript,
        txHash,
      },
      { baseURL: `https://${url}/api/v1` },
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const data = response.data.data

    try {
      return { kind: "ok", data }
    } catch (e) {
      return ApiProblems.badData
    }
  }

  async checkTransactionProcessed(
    url: string,
    txUUID: string,
  ): Promise<ExchangeAgentExchangeProcessedResult> {
    const response: any = await this.api.get(
      `/exchange/cross-shard/?uuid=${txUUID}`,
      {},
      { baseURL: `https://${url}/api/v1` },
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const data = response.data.data

    try {
      return { kind: "ok", data }
    } catch (e) {
      return ApiProblems.badData
    }
  }
}
