import { ApisauceInstance } from "apisauce"

import { ApiProblems, getGeneralApiProblem } from "./api-problem"
import { GetLockedUTXOsResult } from "./api.types"

export class LockedUTXOsApi {
  private api: ApisauceInstance

  constructor(api: ApisauceInstance) {
    this.api = api
  }

  async getLockedUTXOs(
    address: string,
    shardIDS: number[],
    from: number,
    to: number,
    offset: number,
    limit: number,
  ): Promise<GetLockedUTXOsResult> {
    const response: any = await this.api.get(
      `/addresses/${address}/locked-utxos/?from=${from}&to=${to}&offset=${offset}&limit=${limit}&shard=${shardIDS.join(
        "&shard=",
      )}`.replace(/[+]/g, "%2B"),
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const data = response.data.data

    try {
      return { kind: "ok", data }
    } catch (e) {
      return ApiProblems.badData
    }
  }
}
