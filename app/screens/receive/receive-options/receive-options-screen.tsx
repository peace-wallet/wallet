import { observer } from "mobx-react-lite"
import { useColorModeValue } from "native-base"
import React, { FC, useEffect, useState } from "react"
import { Controller, useForm } from "react-hook-form"

import { useNavigation, useRoute } from "@react-navigation/native"

import {
  Button,
  Card,
  Header,
  HStack,
  Icon,
  Input,
  Screen,
  ScreenScrollViewContent,
  Select,
  Text,
  VStack,
} from "../../../components/base"
import { translate, TxKeyPath } from "../../../i18n"
import { useStores } from "../../../models"
import { ReceiveOptionsScreenRouteProps } from "./receive-options-screen.props"

export const ReceiveOptionsScreen: FC = observer(() => {
  const route = useRoute<ReceiveOptionsScreenRouteProps>()
  const { amount: initialAmount, shardID: initialShardID } = route.params ?? {}

  const navigation = useNavigation()

  const {
    control,
    watch,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    mode: "onChange",
    shouldFocusError: true,
    shouldUnregister: false,
  })

  const amount = watch("amount")

  const { coinStore, balanceStore } = useStores()
  const { selectedCoin } = coinStore

  const shards = balanceStore.availableBalancesByCoin(selectedCoin)

  const [shardID, setShardID] = useState(initialShardID ?? null)

  const onSubmit = () => {
    navigation.navigate("receiveQR", {
      amount,
      shardID,
    })
  }

  const renderAmountCaption = () => {
    let textTx: TxKeyPath = null

    switch (errors.amount?.type) {
      case "valid":
        textTx = "errors.amountIsInvalid"
        break
      case "min":
        textTx = "errors.amountIsTooLow"
        break
      default:
        textTx = null
        break
    }

    return (
      textTx && (
        <HStack flex={1} px="4" mb="1" alignItems="center" space={2}>
          <Icon.Info size={3.5} color="red.500" />
          <Text textTx={textTx} fontSize="sm" color="red.500" flex={1} />
        </HStack>
      )
    )
  }

  useEffect(() => {
    setValue(
      "amount",
      initialAmount && isNaN(parseFloat(initialAmount)) ? undefined : initialAmount,
    )
  }, [])

  return (
    <Screen
      header={<Header title={`${translate("titles.receive")} ${selectedCoin.title}`} />}
      unsafeBottom
    >
      <ScreenScrollViewContent
        footer={
          <VStack space="md">
            <Button textTx="buttons.accept" onPress={handleSubmit(onSubmit)} />
          </VStack>
        }
      >
        <VStack space="xs">
          <Card>
            <Controller
              control={control}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input
                  variant="unstyled"
                  keyboardType="numeric"
                  placeholderTx="placeholders.enterAmount"
                  value={value}
                  px="0"
                  py="2"
                  size="lg"
                  error={errors.amount}
                  onBlur={onBlur}
                  onChangeText={(value) => onChange(value.replace(",", "."))}
                />
              )}
              name="amount"
              rules={{
                required: false,
                min: 0.00000001,
                validate: {
                  valid: (value) =>
                    value && value.length > 0
                      ? (selectedCoin.name === "jax"
                          ? /^\d+(\.\d{1,4})?$/
                          : /^\d+(\.\d{1,8})?$/
                        ).test(value)
                      : true,
                },
              }}
            />
          </Card>

          {renderAmountCaption()}

          {selectedCoin.name === "jax" && (
            <Card>
              <VStack flex={1}>
                <Select
                  px="0"
                  py="2"
                  variant="unstyled"
                  placeholderTx="placeholders.selectShard"
                  dropdownCloseIcon={<Icon.ChevronDown />}
                  dropdownOpenIcon={<Icon.ChevronTop />}
                  selectedValue={shardID?.toString()}
                  defaultValue={initialShardID?.toString()}
                  onValueChange={(value) => setShardID(value)}
                >
                  {shards.map((balanceByShard) => (
                    <Select.Item
                      key={`select-shard-${balanceByShard.shardID}`}
                      label={`${translate("labels.shard")} ${balanceByShard.shardID}`}
                      value={balanceByShard.shardID.toString()}
                    />
                  ))}
                </Select>
                <Text
                  textTx="labels.shard"
                  fontSize="xs"
                  color={useColorModeValue("primaryAlpha.300", "whiteAlpha.300")}
                />
              </VStack>
            </Card>
          )}
        </VStack>
      </ScreenScrollViewContent>
    </Screen>
  )
})
