import { observer } from "mobx-react-lite"
import { useColorModeValue, useToken } from "native-base"
import React, { FC, useEffect, useState } from "react"
import { Dimensions, Share } from "react-native"
import QRCode from "react-native-qrcode-svg"
import numeral from "numeral"
import Clipboard from "@react-native-community/clipboard"
import { useNavigation } from "@react-navigation/core"
import { useRoute } from "@react-navigation/native"

import {
  Button,
  Card,
  Header,
  HStack,
  Icon,
  IconButton,
  Screen,
  ScreenScrollViewContent,
  Text,
  Toast,
  VStack,
} from "../../../components/base"
import { translate } from "../../../i18n"
import { useStores } from "../../../models"
import { ReceiveQRScreenRouteProps } from "./receive-qr-screen.props"

export const ReceiveQRScreen: FC = observer(() => {
  const route = useRoute<ReceiveQRScreenRouteProps>()
  const { amount: initialAmount, shardID: initialShardID } = route.params ?? {}

  const navigation = useNavigation()

  const { coinStore, walletStore, networkName } = useStores()
  const { selectedCoin } = coinStore
  const { address } = walletStore

  const [amount, setAmount] = useState(null)
  const [shardID, setShardID] = useState(null)

  const url = `https://explore.jax.net/shards/addresses/${address}?network=${networkName}`

  let qrData = `peacewallet://send/${selectedCoin.name}/${address}`

  if (amount) {
    qrData += `/${amount}`
  }

  if (shardID) {
    qrData += `/${shardID}`
  }

  const onNavigateToEditOptions = async () => {
    navigation.navigate("receiveOptions", {
      amount,
      shardID,
    })
  }

  const onCopy = () => {
    Clipboard.setString(address)
    Toast.show({
      type: "info",
      text1: translate("notifications.copied"),
      text2: address,
    })
  }

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `Jax Explorer | ${address} \n\n${url}`,
      })
      if (result.action === Share.sharedAction) {
        Toast.show({
          type: "info",
          text1: translate("notifications.shared"),
          text2: address,
        })
      }
    } catch (error) {}
  }

  const onClear = () => {
    setAmount(null)
    setShardID(null)
  }

  useEffect(() => {
    setAmount(isNaN(parseFloat(initialAmount)) ? undefined : initialAmount)
    setShardID(initialShardID)
  }, [initialAmount, initialShardID])

  return (
    <Screen
      header={
        <Header
          title={`${translate("titles.receive")} ${selectedCoin.title}`}
          rightAction={
            (amount || shardID) && (
              <IconButton variant="unstyled" icon={<Icon.Close />} onPress={onClear} />
            )
          }
        />
      }
      unsafeBottom
    >
      <ScreenScrollViewContent
        footer={
          <VStack space="md">
            <Button textTx="buttons.createInvoice" onPress={onNavigateToEditOptions} />
          </VStack>
        }
      >
        <VStack space="xs">
          {(amount || shardID) && (
            <Card p="4">
              <HStack>
                {amount && (
                  <VStack flex={1}>
                    <HStack space={1}>
                      <Text
                        text={`${amount > 10000 ? numeral(amount).format("0.00a") : amount}`}
                        fontSize={28}
                        color={`coins.${selectedCoin.name}.500`}
                      />
                    </HStack>
                    <Text textTx="labels.amount" secondary />
                  </VStack>
                )}
                {shardID && (
                  <VStack flex={1}>
                    <HStack space={1}>
                      <Text text={`${translate("labels.shard")} ${shardID}`} fontSize={28} />
                    </HStack>
                    <Text textTx="labels.shard" secondary />
                  </VStack>
                )}
              </HStack>
            </Card>
          )}

          <Card p="2">
            <VStack space={8}>
              <HStack px="2" pt="1">
                <Text textTx="labels.qrCode" secondary />
              </HStack>

              <HStack alignItems="center" justifyContent="center">
                <QRCode
                  color={useToken("colors", useColorModeValue("primary.500", "white"))}
                  backgroundColor={useToken("colors", useColorModeValue("white", "darkGray.500"))}
                  size={Dimensions.get("screen").width * 0.6}
                  value={qrData}
                />
              </HStack>

              <HStack px="2" justifyContent="center">
                <Text text={address} secondary />
              </HStack>

              <HStack space="xs">
                <Button
                  flex={1}
                  textTx="buttons.copy"
                  variant="outline"
                  borderRadius="md"
                  endIcon={<Icon.Copy />}
                  onPress={onCopy}
                />
                <Button
                  flex={1}
                  textTx="buttons.share"
                  variant="outline"
                  borderRadius="md"
                  endIcon={<Icon.Share />}
                  onPress={onShare}
                />
              </HStack>
            </VStack>
          </Card>
        </VStack>
      </ScreenScrollViewContent>
    </Screen>
  )
})
