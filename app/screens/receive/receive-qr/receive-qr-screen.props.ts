import { RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { ReceiveNavigatorParamList } from "../../../navigators/stacks"

export type ReceiveQRScreenRouteProps = RouteProp<ReceiveNavigatorParamList, "receiveQR">
export type ReceiveQRScreenNavigationProps = StackNavigationProp<
  ReceiveNavigatorParamList,
  "receiveQR"
>
