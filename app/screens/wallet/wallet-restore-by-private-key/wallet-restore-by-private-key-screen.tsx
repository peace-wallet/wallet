import { observer } from "mobx-react-lite"
import React from "react"
import { Controller, useForm } from "react-hook-form"

import Clipboard from "@react-native-community/clipboard"
import { StackActions, useNavigation } from "@react-navigation/native"

import {
  Button,
  Card,
  Header,
  HStack,
  Icon,
  Screen,
  ScreenContent,
  Spacer,
  Text,
  TextArea,
  VStack,
} from "../../../components/base"
import { TxKeyPath } from "../../../i18n"
import { useStores } from "../../../models"

export const WalletRestoreByPrivateKeyScreen = observer(() => {
  const navigation = useNavigation()
  const rootStore = useStores()
  const { walletStore } = rootStore

  const {
    control,
    setValue,
    trigger,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    mode: "all",
  })

  const privateKey = watch("privateKey")

  const onSubmit = async (data) => {
    const { privateKey } = data

    if (walletStore.isValidPrivateKey(privateKey)) {
      await walletStore.savePrivateKey(privateKey)
    } else {
      await walletStore.saveWIF(privateKey)
    }

    rootStore.showPinCodeScreen()

    navigation.dispatch(
      StackActions.replace("tabsStack", {
        screen: "balance",
      }),
    )
  }

  const onPasteFromClipboard = async () => {
    const text = (await Clipboard.getString()).replace(/\n/g, " ").trim()
    setValue("privateKey", text)
    trigger("privateKey")
  }

  const renderPrivateKeyCaption = () => {
    let textTx: TxKeyPath = null

    switch (errors.privateKey?.type) {
      case "required":
        textTx = "errors.privateKeyIsRequired"
        break
      case "valid":
        textTx = "errors.privateKeyIsInvalid"
        break
      default:
        textTx = null
        break
    }

    return (
      textTx && (
        <HStack px="4" alignItems="center" space={2}>
          <Icon.Info size={3.5} color="red.500" />
          <Text textTx={textTx} fontSize="sm" color="red.500" />
        </HStack>
      )
    )
  }

  return (
    <Screen testID="wallet-restore-screen" header={<Header />}>
      <ScreenContent>
        <VStack flex={1} space="sm">
          <Text textTx="titles.walletRestore" fontSize={28} align="center" />

          <VStack space="xs">
            <Card error={errors.privateKey}>
              <Controller
                control={control}
                render={({ field: { onChange, onBlur, value } }) => (
                  <TextArea
                    value={value}
                    variant="unstyled"
                    totalLines={3}
                    p="0"
                    size="lg"
                    error={errors.privateKey}
                    placeholderTx="placeholders.walletRestoreByPrivateKey"
                    onBlur={onBlur}
                    onChangeText={onChange}
                  />
                )}
                name="privateKey"
                rules={{
                  required: true,
                  validate: {
                    valid: (value) => {
                      return walletStore.isValidPrivateKey(value) || walletStore.isValidWIF(value)
                    },
                  },
                }}
              />
            </Card>

            {renderPrivateKeyCaption()}

            <HStack justifyContent="center">
              <Button
                textTx="buttons.paste"
                size="md"
                variant="subtle"
                startIcon={<Icon.Paste />}
                onPress={onPasteFromClipboard}
              />
            </HStack>
          </VStack>

          <Spacer />

          <Button
            isDisabled={!privateKey || errors.privateKey}
            textTx="buttons.restoreWallet"
            isLoading={walletStore.isLoading}
            onPress={handleSubmit(onSubmit)}
          />
        </VStack>
      </ScreenContent>
    </Screen>
  )
})
