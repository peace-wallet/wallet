import Clipboard, { useClipboard } from "@react-native-community/clipboard"
import { by, device, element, expect } from "detox"
import { array } from "mobx-state-tree/dist/internal"
import { values } from "ramda"
import { Value } from "react-native-reanimated"
import { MnemonicPhrase } from "../../../components/features"

describe("Onboarding", () => {
  beforeAll(async () => {
    await device.launchApp()
  })

  beforeEach(async () => {
    await device.reloadReactNative()
  })

  it("should show onboarding screen", async () => {
    await expect(element(by.id("onboarding-screen"))).toBeVisible()
  })


  it("Check back button in 'create-wallet-screen'", async () => {
    await element(by.id("create-wallet-button")).tap()
    await expect(element(by.id("wallet-create-screen"))).toBeVisible()
    await element(by.id("navigation-back-button")).tap()
    await expect(element(by.id("onboarding-screen"))).toBeVisible()
  })

  it("Check copy button",async () => {
    await element(by.id("create-wallet-button")).tap()
    await expect(element(by.id("wallet-create-screen"))).toBeVisible()
    await element(by.id("mnemonic-words-0"))
    await element(by.id("copy-Phrase")).tap()
    var screenMnemonic = []
    for (let i = 0; i <= 11; i++) {
      let actualWord = await element(by.id(`mnemonic-words-${i}`)).getAttributes()
      screenMnemonic.push(actualWord['text'])
  }
    await element(by.id("wallet-create-screen")).swipe("right")
    await element(by.id("restore-wallet-button")).tap()
    await element(by.id("pasteButton")).tap()
    var actualMnemonic = await element(by.id("TextArea")).getAttributes()
    var mnemonic = actualMnemonic['text']
    await expect(element(by.text(mnemonic.toString()))).toHaveText(screenMnemonic.join(" " ))

    
  })
})
