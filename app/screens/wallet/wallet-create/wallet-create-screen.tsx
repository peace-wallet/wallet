import React, { useState } from "react"
import Clipboard from "@react-native-community/clipboard"
import { StackActions, useNavigation } from "@react-navigation/native"

import {
  Button,
  Header,
  HStack,
  Icon,
  Screen,
  ScreenContent,
  Spacer,
  Text,
  Toast,
  VStack,
} from "../../../components/base"
import { MnemonicPhrase } from "../../../components/features"
import { translate } from "../../../i18n"
import { useStores } from "../../../models"

export const WalletCreateScreen = () => {
  const navigation = useNavigation()
  const rootStore = useStores()
  const { walletStore } = rootStore

  const [mnemonic, setMnemonic] = useState(walletStore.generateMnemonic())

  const navigateToBalanceScreen = async () => {
    await walletStore.saveMnemonic(mnemonic)

    rootStore.showPinCodeScreen()

    navigation.dispatch(
      StackActions.replace("tabsStack", {
        screen: "balance",
      }),
    )
  }

  const onCopyPhrase  = () => { 
    Clipboard.setString(mnemonic)
    
    Toast.show({
      
      type: "info",
      text1: translate("notifications.copied"),
    })
  }

  const onNewPhrase = () => setMnemonic(walletStore.generateMnemonic())

  return (
    <Screen testID="wallet-create-screen" header={<Header />}>
      <ScreenContent >
        <VStack flex={1} space="lg">
          <Text textTx="titles.walletCreate" fontSize={28} align="center" />

          <HStack justifyContent="center" px="2">
            <Text textTx="texts.walletCreate" align="center" secondary />
          </HStack>

          <MnemonicPhrase mnemonic={mnemonic} />

          <Spacer />

          <VStack space="md">
            <HStack space="sm">
              <Button
                testID="copy-Phrase"
                textTx="buttons.copyPhrase"
                borderRadius="md"
                flex={1}
                variant="outline"
                endIcon={<Icon.Copy testID='IconTest'/>}
                onPress={onCopyPhrase}
              />
              <Button
                textTx="buttons.newPhrase"
                borderRadius="md"
                flex={1}
                variant="outline"
                endIcon={<Icon.Refresh />}
                onPress={onNewPhrase}
              />
            </HStack>
            <Button
              textTx="buttons.savePhrase"
              isLoading={walletStore.isLoading}
              onPress={navigateToBalanceScreen}
            />
          </VStack>
        </VStack>
      </ScreenContent>
    </Screen>
  )
}
