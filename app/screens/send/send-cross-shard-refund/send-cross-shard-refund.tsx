import React from "react"
import { Controller, useForm } from "react-hook-form"

import Clipboard from "@react-native-community/clipboard"
import { useNavigation, useRoute } from "@react-navigation/native"

import {
  Button,
  Card,
  Header,
  HStack,
  Icon,
  Screen,
  ScreenContent,
  Spacer,
  Text,
  TextArea,
  VStack
} from "../../../components/base"
import { RefundButton } from "../../../components/features"
import { useStores } from "../../../models"
import { SentCrossShardRefundScreenRouteProps } from "./send-cross-shard-refund.props"

export const SentCrossShardRefundScreen = () => {
  const navigation = useNavigation()

  const route = useRoute<SentCrossShardRefundScreenRouteProps>()
  const { shardID, amount, transactionFee } = route.params ?? {}

  const { control, watch, setValue, trigger } = useForm({
    mode: "onChange",
    shouldFocusError: true,
  })

  const refundHex = watch("refundHex")

  const { walletStore } = useStores()

  const onPasteFromClipboard = async () => {
    const text = (await Clipboard.getString()).replace(/\n/g, " ").trim()
    setValue("refundHex", text)
    trigger("refundHex")
  }

  const onRefund = (hash: string) => {
    navigation.navigate("sendStack", {
      screen: "sentSuccessfully",
      params: {
        address: walletStore.address,
        sourceShardID: shardID,
        sourceAmount: amount,
        transactionFee: transactionFee,
        transactionHash: hash,
      },
    })
  }

  return (
    <Screen header={<Header />}>
      <ScreenContent>
        <VStack flex={1} space="lg">
          <Text textTx="titles.refundTransaction" fontSize={28} align="center" />

          <HStack justifyContent="center" px="2">
            <Text textTx="texts.refundTransaction" align="center" secondary />
          </HStack>

          <VStack space="xs">
            <Card>
              <VStack>
                <Controller
                  control={control}
                  render={({ field: { onChange, onBlur, value } }) => (
                    <TextArea
                      value={value}
                      variant="unstyled"
                      totalLines={3}
                      p="0"
                      size="lg"
                      placeholderTx="placeholders.refundHex"
                      onBlur={onBlur}
                      onChangeText={onChange}
                    />
                  )}
                  name="refundHex"
                  rules={{
                    required: true,
                    validate: {
                      valid: (value) => walletStore.isValidAddress(value),
                    },
                  }}
                />
              </VStack>
            </Card>

            <HStack justifyContent="center">
              <Button
                textTx="buttons.paste"
                size="md"
                variant="outline"
                _dark={{ backgroundColor: "darkGray.500" }}
                startIcon={<Icon.Paste />}
                onPress={onPasteFromClipboard}
              />
            </HStack>
          </VStack>

          <Spacer />

          <RefundButton shardID={shardID} transactionHex={refundHex} onRefund={onRefund} />
        </VStack>
      </ScreenContent>
    </Screen>
  )
}
