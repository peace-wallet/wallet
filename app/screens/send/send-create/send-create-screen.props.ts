import { RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { SendNavigatorParamList } from "../../../navigators/stacks"

export type SendCreateScreenRouteProps = RouteProp<SendNavigatorParamList, "sendCreate">
export type SendCreateScreenNavigationProps = StackNavigationProp<
  SendNavigatorParamList,
  "sendCreate"
>
