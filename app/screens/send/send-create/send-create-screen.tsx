import { observer } from "mobx-react-lite"
import { useColorModeValue } from "native-base"
import React, { FC, useEffect, useRef, useState } from "react"
import { Controller, useForm } from "react-hook-form"
import { Dimensions, Keyboard } from "react-native"

import Clipboard from "@react-native-community/clipboard"
import { useNavigation, useRoute } from "@react-navigation/native"

import {
  Box,
  Button,
  Card,
  Center,
  Divider,
  Header,
  HStack,
  Icon,
  Input,
  Link,
  Modal,
  Pressable,
  RefreshControl,
  Screen,
  ScreenScrollViewContent,
  Select,
  Spinner,
  Text,
  Toast,
  VStack,
} from "../../../components/base"
import { FeeSelect, TransactionDetails } from "../../../components/features"
import { translate, TxKeyPath } from "../../../i18n"
import { ExchangeAgent, useStores } from "../../../models"
import { HistoryCrossShardTempModel } from "../../../models/history-cross-shard-temp/history-cross-shard-temp"
import { HistoryItemModel } from "../../../models/history-item/history-item"
import { delay } from "../../../utils/delay"
import { CoinsFormatter } from "../../../utils/formatters"
import { SendCreateScreenRouteProps } from "./send-create-screen.props"

export const SendCreateScreen: FC = observer(() => {
  const navigation = useNavigation()

  const route = useRoute<SendCreateScreenRouteProps>()
  const {
    coinName: initialCoinName,
    address: initialAddress,
    amount: initialAmount,
    shardID: initialShardID,
  } = route.params ?? {}

  const {
    control,
    watch,
    setValue,
    handleSubmit,
    formState: { errors, dirtyFields, touchedFields },
  } = useForm({
    mode: "onChange",
    shouldFocusError: true,
  })

  const {
    walletStore,
    balanceStore,
    feeStore,
    transactionStore,
    coinStore,
    historyStore,
    exchangeAgentStore,
  } = useStores()
  const { coins, selectCoin } = coinStore
  const { fees, selectedFee, selectFee } = feeStore
  const { fees: utxosFees } = transactionStore
  const {
    selectedExchangeAgent,
    selectExchangeAgent,
    exchangeAgentsSortedByLowestFee,
  } = exchangeAgentStore

  const shardIDs = [
    ...balanceStore
      .availableBalancesByCoin(coinStore.coins.find((coin) => coin.name === "jax"))
      .map((balance) => balance.shardID),
  ]

  const jaxShardIDIsValid =
    initialCoinName === "jax" ? shardIDs.includes(Number(initialShardID)) : true
  const initialCoinNameIsValid = initialCoinName
    ? coins.find((c) => c.name === initialCoinName)
    : false
  const initialAmountIsValid = initialAmount
    ? !isNaN(Number(initialAmount)) && Number(initialAmount) > 0
    : true
  const initialShardIDIsValid = initialShardID
    ? !isNaN(Number(initialShardID)) && Number(initialShardID) >= 0 && jaxShardIDIsValid
    : true

  const selectedCoin = coins.find((c) => c.name === initialCoinName)

  const firstAvailableBalance = balanceStore.availableBalancesByCoin(selectedCoin, {
    excludeEmpty: true,
  })[0]

  const amount = watch("amount")
  const address = watch("address")

  const amountEnteringTimer = useRef(null)

  const [coinBalance, setCoinBalance] = useState(0)
  const [sourceShardID, setSourceShardID] = useState(undefined)
  const [destinationShardID, setDestinationShardID] = useState(undefined)
  const [fee, setFee] = useState(null)
  const [maxAmount, setMaxAmount] = useState(null)

  const [amountIsEdited, setAmountIsEdited] = useState(false)
  const [feeIsLoading, setFeeIsLoading] = useState(false)
  const [refreshingExchangeAgents, setRefreshingExchangeAgents] = useState(false)

  const [confirmationModalIsVisible, setConfirmationModalIsVisible] = useState(false)
  const [insufficientAmountModalIsVisible, setInsufficientAmountModalIsVisible] = useState(false)
  const [tooManyUTXOsModalIsVisible, setTooManyUTXOsModalIsVisible] = useState(false)
  const [exchangeAgentsModalIsVisible, setExchangeAgentsModalIsVisible] = useState(false)

  const [routeFocusHash, setRouteFocusHash] = useState(null)

  const isRegularTransaction = sourceShardID === destinationShardID

  const navigateToSendScanQRScreen = () => {
    navigation.navigate("sendScanQR", {
      sendCoinName: selectedCoin.name,
      formIsDirty: Object.keys(dirtyFields).length > 0,
      cached: {
        coinName: selectedCoin.name,
        address: errors.address ? null : address,
        amount: errors.amount ? null : amount,
        shardID: destinationShardID,
      },
    })
  }

  const onPasteAddressFromClipboard = async () => {
    const text = (await Clipboard.getString()).replace(/\n/g, "").trim()
    setValue("address", text, { shouldValidate: true, shouldDirty: true })
  }

  const createRegularTransaction = async () => {
    const [data, error] = await transactionStore.createRegularTransaction(
      sourceShardID,
      address,
      CoinsFormatter.toSmallestUnits(selectedCoin.name, amount),
      fee,
    )

    if (error) {
      if (
        error.source &&
        (error.source.code === 104 || error.source.code === 105 || error.source.code === 106)
      ) {
        Toast.show({
          type: "info",
          text1: translate("notifications.feeChangedTitle"),
          text2: translate("notifications.feeChangedDescription"),
        })
        setConfirmationModalIsVisible(false)
        setValue("amount", "", { shouldValidate: true })
      } else if (error.source && error.source.code === 109) {
        Toast.show({
          type: "error",
          text1: translate("notifications.error"),
          text2: translate("notifications.transactionAlreadyPublished"),
        })
      } else {
        Toast.show({
          type: "error",
          text1: translate("notifications.error"),
          text2: `${transactionStore.error.message}`,
        })
      }
    } else {
      const { hash } = data
      setConfirmationModalIsVisible(false)
      historyStore.addOrUpdateLocal(
        sourceShardID,
        HistoryItemModel.create({
          address: address,
          sourceAmount: CoinsFormatter.toSmallestUnits(selectedCoin.name, amount) + fee,
          destinationAmount: CoinsFormatter.toSmallestUnits(selectedCoin.name, amount) + fee,
          transactionFee: fee,
          confirmations: 0,
          direction: "out",
          sourceShardID: sourceShardID,
          destinationShardID: sourceShardID,
          timestamp: new Date().toUTCString(),
          transactionHash: hash,
          blockNumber: null,
          crossShardTemp: null,
        }),
      )
      navigation.reset({
        routes: [
          {
            name: "sentSuccessfully",
            params: {
              address,
              sourceShardID,
              sourceAmount: CoinsFormatter.toSmallestUnits(selectedCoin.name, amount),
              destinationAmount: CoinsFormatter.toSmallestUnits(selectedCoin.name, amount),
              transactionFee: fee,
              transactionHash: hash,
            },
          },
        ],
      })
    }
  }

  const createCrossShardTransaction = async () => {
    const tempTxHash = `${amount}:${sourceShardID}:${destinationShardID}:${address}:${selectedExchangeAgent.publicKey}`

    const historyItem = HistoryItemModel.create({
      address,
      sourceAmount: CoinsFormatter.toSmallestUnits(selectedCoin.name, amount),
      destinationAmount:
        CoinsFormatter.toSmallestUnits(selectedCoin.name, amount) -
        selectedExchangeAgent?.proposal?.calculateFee(
          CoinsFormatter.toSmallestUnits(selectedCoin.name, amount),
        ),
      transactionFee: fee,
      exchangeAgentFee: selectedExchangeAgent?.proposal?.calculateFee(
        CoinsFormatter.toSmallestUnits(selectedCoin.name, amount),
      ),
      confirmations: 0,
      direction: "out",
      sourceShardID,
      destinationShardID,
      timestamp: new Date().toUTCString(),
      transactionHash: tempTxHash,
      blockNumber: null,
      crossShardTemp: HistoryCrossShardTempModel.create({
        exchangeAgentUrl: selectedExchangeAgent.addresses[0].url,
        exchangeAgentPublicKey: selectedExchangeAgent.publicKey,
      }),
    })

    setConfirmationModalIsVisible(false)

    historyStore.addOrUpdateLocal(sourceShardID, historyItem)

    await delay(150)

    navigation.reset({
      routes: [
        {
          name: "sendCrossShardProcessing",
          params: {
            sourceShardID,
            destinationShardID,
            transactionHash: tempTxHash,
          },
        },
      ],
    })
  }

  const onSubmit = () => {
    isRegularTransaction
      ? setConfirmationModalIsVisible(true)
      : setExchangeAgentsModalIsVisible(true)
  }

  const onConfirm = () => {
    if (isRegularTransaction) {
      createRegularTransaction()
    } else {
      createCrossShardTransaction()
    }
  }

  const onRefreshExchangeAgents = async () => {
    setRefreshingExchangeAgents(true)
    await getExchangeAgents()
    setRefreshingExchangeAgents(false)
  }

  const getUTXOsAndFees = (amount: number | string) => {
    if (sourceShardID === null || sourceShardID === undefined) return

    if (!amount || amount <= 0 || errors.amount !== undefined) return

    setFeeIsLoading(true)
    transactionStore
      .getUTXOsAndFees(
        sourceShardID,
        walletStore.address,
        CoinsFormatter.toSmallestUnits(selectedCoin.name, Number(amount)),
      )
      .then(() => {
        if (transactionStore.isError) {
          if (transactionStore.error.source?.code === 107) {
            Toast.show({
              type: "info",
              text1: translate("notifications.noFreeUTXOTitle"),
              text2: translate("notifications.noFreeUTXODescription"),
              visibilityTime: 15000,
            })
          } else {
            Toast.show({
              type: "error",
              text1: translate("notifications.error"),
              text2: `${transactionStore.error.message}`,
            })
          }
        }

        setAmountIsEdited(false)
        setFeeIsLoading(false)
      })
  }

  const getExchangeAgents = async () => {
    if (
      sourceShardID === null ||
      sourceShardID === undefined ||
      destinationShardID === null ||
      destinationShardID === undefined
    )
      return

    await exchangeAgentStore.getExchangeAgents(sourceShardID, destinationShardID).then(() => {
      if (exchangeAgentStore.error) {
        Toast.show({
          type: "error",
          text1: translate("notifications.error"),
          text2: `${exchangeAgentStore.error.message}`,
        })
      }
    })
  }

  const renderAddressCaption = () => {
    let textTx: TxKeyPath = null

    switch (errors.address?.type) {
      case "required":
        textTx = "errors.addressIsRequired"
        break
      case "valid":
        textTx = "errors.addressIsInvalid"
        break
      default:
        textTx = null
        break
    }

    return (
      textTx && (
        <HStack px="3" alignItems="center" space={2}>
          <Icon.Info size={3.5} color="red.500" />
          <Text textTx={textTx} fontSize="sm" color="red.500" />
        </HStack>
      )
    )
  }

  const renderAmountCaption = () => {
    let textTx: TxKeyPath = null

    if (!coinBalance) textTx = errors.notEnoughBalance

    switch (errors.amount?.type) {
      case "required":
        textTx = "errors.amountIsRequired"
        break
      case "valid":
        textTx = "errors.amountIsInvalid"
        break
      case "min":
        textTx = "errors.amountIsTooLow"
        break
      case "max":
        textTx = "errors.amountIsTooHigh"
        break
      default:
        textTx = null
        break
    }

    return (
      textTx && (
        <HStack flex={1} px="4" mb="1" alignItems="center" space={2}>
          <Icon.Info size={3.5} color="red.500" />
          <Text textTx={textTx} fontSize="sm" color="red.500" flex={1} />
        </HStack>
      )
    )
  }

  const renderExchangeAgentListItem = (item: { item: ExchangeAgent; index }) => (
    <Pressable onPress={() => selectExchangeAgent(item.item)}>
      <Box
        _light={{
          bgColor: item.item.id === selectedExchangeAgent?.id ? "primaryAlpha.50" : "transparent",
        }}
        _dark={{
          bgColor: item.item.id === selectedExchangeAgent?.id ? "whiteAlpha.50" : "transparent",
        }}
      >
        <HStack space="sm" alignItems="center" justifyContent="space-between" px="4" py="3">
          <Text flex={1} fontSize="sm" text={item.item.addresses[0].url} />
          <Text
            text={`${translate("labels.fee")} ${
              item.item.proposal
                ? CoinsFormatter.toBiggestUnits(
                    selectedCoin.name,
                    item.item.proposal?.calculateFee(
                      CoinsFormatter.toSmallestUnits(selectedCoin.name, amount),
                    ),
                  )
                : "N/A"
            } ${selectedCoin.title}`}
            secondary
          />
        </HStack>
      </Box>
    </Pressable>
  )

  const renderInsufficientAmountModal = () => (
    <Modal
      isVisible={insufficientAmountModalIsVisible}
      onClosed={() => setInsufficientAmountModalIsVisible(false)}
      FooterComponent={
        <HStack space="xs">
          <Button
            flex={1}
            textTx="buttons.editAmount"
            variant="ghost"
            onPress={() => setInsufficientAmountModalIsVisible(false)}
          />
          <Button
            flex={1}
            textTx="buttons.accept"
            isLoading={feeIsLoading}
            isDisabled={feeIsLoading}
            onPress={() => setInsufficientAmountModalIsVisible(false)}
          />
        </HStack>
      }
    >
      <Text
        textTx="texts.maximumAmount"
        textTxOptions={{
          coin: selectedCoin.title,
          balance: coinBalance,
          fee: CoinsFormatter.toBiggestUnits(selectedCoin.name, fee),
          maxAmount: CoinsFormatter.toBiggestUnits(selectedCoin.name, maxAmount),
        }}
        align="center"
      />
    </Modal>
  )

  const renderTooManyUTXOsModal = () => (
    <Modal
      isVisible={tooManyUTXOsModalIsVisible}
      onClosed={() => setTooManyUTXOsModalIsVisible(false)}
      FooterComponent={
        <HStack space="xs">
          <Button
            flex={1}
            textTx="buttons.editAmount"
            variant="ghost"
            onPress={() => setTooManyUTXOsModalIsVisible(false)}
          />
          <Button
            flex={1}
            textTx="buttons.accept"
            isLoading={feeIsLoading}
            isDisabled={feeIsLoading}
            onPress={() => setTooManyUTXOsModalIsVisible(false)}
          />
        </HStack>
      }
    >
      <Text
        textTx="texts.tooManyUTXOs"
        textTxOptions={{
          coin: selectedCoin.title,
          balance: coinBalance,
          fee: CoinsFormatter.toBiggestUnits(selectedCoin.name, fee),
          maxAmount: CoinsFormatter.toBiggestUnits(selectedCoin.name, maxAmount),
        }}
        align="center"
      />
    </Modal>
  )

  const renderConfirmationModal = () => (
    <Modal
      isVisible={confirmationModalIsVisible}
      HeaderComponent={<Text textTx="titles.operationDetails" textTransform="uppercase" />}
      FooterComponent={
        <HStack space="xs">
          <Button
            flex={1}
            textTx={isRegularTransaction ? "buttons.cancel" : "buttons.back"}
            variant="ghost"
            onPress={() => setConfirmationModalIsVisible(false)}
          />
          <Button
            flex={1}
            textTx="buttons.confirm"
            isLoading={transactionStore.isLoading}
            onPress={onConfirm}
          />
        </HStack>
      }
      onClosed={() => setConfirmationModalIsVisible(false)}
    >
      <TransactionDetails
        coin={selectedCoin}
        sourceAmount={CoinsFormatter.toSmallestUnits(selectedCoin.name, amount)}
        destinationAmount={
          CoinsFormatter.toSmallestUnits(selectedCoin.name, amount) -
          selectedExchangeAgent?.proposal?.calculateFee(
            CoinsFormatter.toSmallestUnits(selectedCoin.name, amount),
          )
        }
        address={address}
        sourceShardID={sourceShardID}
        destinationShardID={destinationShardID}
        transactionFee={fee}
        exchangeAgentFee={selectedExchangeAgent?.proposal?.calculateFee(
          CoinsFormatter.toSmallestUnits(selectedCoin.name, amount),
        )}
        exchangeAgentUrl={selectedExchangeAgent?.addresses[0].url}
      />
    </Modal>
  )

  const renderExchangeAgentsModal = () => (
    <Modal
      isVisible={exchangeAgentsModalIsVisible}
      modalHeight={Dimensions.get("screen").height * 0.5}
      adjustToContentHeight={false}
      panGestureEnabled={false}
      HeaderComponent={
        <VStack space="sm">
          <Text textTx="texts.chooseExchangeAgent" align="center" />
          <HStack alignItems="center" justifyContent="center">
            <Link url="https://peacewallet.io" textTx="buttons.readMore" />
          </HStack>
        </VStack>
      }
      FooterComponent={
        <HStack space="xs">
          <Button
            flex={1}
            textTx="buttons.cancel"
            variant="ghost"
            onPress={() => setExchangeAgentsModalIsVisible(false)}
          />
          <Button
            flex={1}
            textTx="buttons.next"
            disabled={!selectedExchangeAgent}
            onPress={() => {
              setExchangeAgentsModalIsVisible(false)
              setConfirmationModalIsVisible(true)
            }}
          />
        </HStack>
      }
      flatListProps={{
        data: exchangeAgentsSortedByLowestFee(
          CoinsFormatter.toSmallestUnits(selectedCoin.name, amount),
        ),
        refreshControl: (
          <RefreshControl
            refreshing={refreshingExchangeAgents || exchangeAgentStore.isLoading}
            onRefresh={onRefreshExchangeAgents}
          />
        ),
        renderItem: renderExchangeAgentListItem,
        keyExtractor: (item, index) => `exchange-agent${index}`,
        ListEmptyComponent: !exchangeAgentStore.isLoading && (
          <Center pt="10">
            <Text textTx="texts.emptyExchangeAgents" align="center" />
          </Center>
        ),
      }}
      onClosed={() => setExchangeAgentsModalIsVisible(false)}
    />
  )

  useEffect(() => {
    if (initialCoinNameIsValid) {
      selectCoin(coins.find((c) => c.name === initialCoinName))

      if (initialCoinName === "jxn") {
        setSourceShardID(0)
        setDestinationShardID(0)
      }
    }
  }, [initialCoinName])

  useEffect(() => {
    if (!initialCoinNameIsValid || !initialAmountIsValid || !initialShardIDIsValid) {
      navigation.canGoBack() && navigation.goBack()

      if (walletStore.mnemonic || walletStore.privateKey || walletStore.wif) {
        Toast.show({
          type: "error",
          text1: translate("notifications.error"),
          text2: translate("notifications.invalidQRCode"),
        })
      }

      return
    }

    if (initialAddress !== null && initialAddress !== undefined) {
      setValue("address", initialAddress, {
        shouldValidate: true,
      })
    }

    if (initialAmount !== null && initialAmount !== undefined) {
      setValue("amount", initialAmount, { shouldValidate: true })
    }

    if (initialShardID !== null && initialShardID !== undefined && initialCoinName === "jax") {
      setSourceShardID(initialShardID)
      setDestinationShardID(initialShardID)
    } else {
      setSourceShardID(firstAvailableBalance?.shardID ?? selectedCoin.shardID ?? 1)
      setDestinationShardID(selectedCoin.shardID ?? 1)
    }

    setSourceShardID(firstAvailableBalance?.shardID ?? selectedCoin.shardID ?? 1)
  }, [initialAddress, initialAmount, initialShardID, routeFocusHash])

  useEffect(() => {
    const balance = balanceStore.balanceByCoin(
      selectedCoin,
      selectedCoin.name === "jax" && parseInt(sourceShardID),
    ).available
    setCoinBalance(balance ?? firstAvailableBalance?.availableBalance ?? 0)

    if (isRegularTransaction) getUTXOsAndFees(amount)
    else getExchangeAgents()
  }, [sourceShardID])

  useEffect(() => {
    getExchangeAgents()
  }, [destinationShardID])

  useEffect(() => {
    if (
      Number(maxAmount) === CoinsFormatter.toSmallestUnits(selectedCoin.name, amount) ||
      errors.amount !== undefined
    )
      return

    setAmountIsEdited(true)
    clearTimeout(amountEnteringTimer.current)
    amountEnteringTimer.current = setTimeout(() => {
      getUTXOsAndFees(amount)
    }, 1000)

    return () => {
      clearTimeout(amountEnteringTimer.current)
    }
  }, [amount, selectedFee])

  useEffect(() => {
    setMaxAmount(null)

    const utxoFee = utxosFees.find((utxoFee) => utxoFee.name === selectedFee.name)

    if (!utxoFee || feeIsLoading) return

    const utxoFeeMaxAmountFormatted = CoinsFormatter.toBiggestUnits(
      selectedCoin.name,
      utxoFee.maxAmount,
    )

    setFee(utxoFee.value)
    setMaxAmount(utxoFee.maxAmount)

    if (amount > utxoFeeMaxAmountFormatted) {
      setValue("amount", `${utxoFeeMaxAmountFormatted}`)

      if (transactionStore.code === 0) {
        setInsufficientAmountModalIsVisible(true)
      } else {
        setTooManyUTXOsModalIsVisible(true)
      }
    }

    return () => {
      setMaxAmount(null)
    }
  }, [selectedFee, feeIsLoading, utxosFees])

  useEffect(() => {
    if (!insufficientAmountModalIsVisible) {
      setMaxAmount(null)
    }
  }, [insufficientAmountModalIsVisible])

  useEffect(() => {
    if (exchangeAgentsModalIsVisible) selectExchangeAgent(null)
  }, [exchangeAgentsModalIsVisible])

  useEffect(() => {
    if (confirmationModalIsVisible) {
      Keyboard.dismiss()
    }
  }, [confirmationModalIsVisible])

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", (params) => {
      setRouteFocusHash(`${params.target}-${new Date()}`)
    })

    return unsubscribe
  }, [navigation])

  useEffect(() => {
    async function getFee() {
      await getUTXOsAndFees(initialAmount)
    }

    if (initialAmount) getFee()

    return () => {
      selectExchangeAgent(null)
      transactionStore.clearFees()
    }
  }, [])

  return (
    <Screen header={<Header title={`${translate("titles.send")} ${selectedCoin.title}`} />}>
      <ScreenScrollViewContent
        footer={
          <Button
            textTx="buttons.send"
            isDisabled={amountIsEdited || feeIsLoading || !coinBalance || !fee || errors.amount}
            onPress={handleSubmit(onSubmit)}
          />
        }
      >
        {renderExchangeAgentsModal()}
        {renderConfirmationModal()}
        {renderInsufficientAmountModal()}
        {renderTooManyUTXOsModal()}

        <VStack space="xs">
          <Card error={errors.address}>
            <Controller
              control={control}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input
                  variant="unstyled"
                  placeholderTx="placeholders.receiverAddress"
                  value={value}
                  px="0"
                  py="2"
                  size="lg"
                  error={errors.address}
                  InputRightElement={
                    <Pressable onPress={navigateToSendScanQRScreen}>
                      <Icon.ScanQR ml="2" />
                    </Pressable>
                  }
                  onBlur={onBlur}
                  onChangeText={onChange}
                />
              )}
              name="address"
              rules={{
                required: true,
                validate: {
                  valid: (value) => walletStore.isValidAddress(value),
                },
              }}
              defaultValue={initialAddress}
            />
          </Card>

          {renderAddressCaption()}

          <HStack justifyContent="center" mb="1">
            <Button
              textTx="buttons.paste"
              size="md"
              variant="subtle"
              startIcon={<Icon.Paste />}
              onPress={onPasteAddressFromClipboard}
            />
          </HStack>

          {selectedCoin.name === "jax" && (
            <Card>
              <HStack space="xs" alignItems="center">
                <VStack flex={1}>
                  <Select
                    px="0"
                    py="2"
                    variant="unstyled"
                    dropdownCloseIcon={<Icon.ChevronDown />}
                    dropdownOpenIcon={<Icon.ChevronTop />}
                    selectedValue={sourceShardID?.toString()}
                    onValueChange={(value) => {
                      setSourceShardID(parseInt(value))
                      setDestinationShardID(parseInt(value))
                    }}
                  >
                    {balanceStore
                      .availableBalancesByCoin(selectedCoin, { excludeEmpty: true })
                      .map((balanceByShard) => (
                        <Select.Item
                          key={`select-shard-${balanceByShard.shardID}`}
                          label={`${translate("labels.shard")} ${balanceByShard.shardID}`}
                          value={balanceByShard.shardID.toString()}
                        />
                      ))}
                  </Select>
                  <Text
                    textTx="labels.sourceShard"
                    fontSize="xs"
                    color={useColorModeValue("primaryAlpha.300", "whiteAlpha.300")}
                  />
                </VStack>
                {/* <Divider height="100%" orientation="vertical" mr={1} /> */}

                {/* <VStack flex={1}>
                  <Select
                    px="0"
                    py="2"
                    variant="unstyled"
                    dropdownCloseIcon={<Icon.ChevronDown />}
                    dropdownOpenIcon={<Icon.ChevronTop />}
                    selectedValue={destinationShardID?.toString()}
                    onValueChange={(value) => setDestinationShardID(parseInt(value))}
                  >
                    {balanceStore.availableBalancesByCoin(selectedCoin).map((balanceByShard) => (
                      <Select.Item
                        key={`destination-shard-${balanceByShard.shardID}`}
                        value={balanceByShard.shardID.toString()}
                        label={`${translate("labels.shard")} ${balanceByShard.shardID}`}
                      />
                    ))}
                  </Select>
                  <Text
                    textTx="labels.destinationShard"
                    fontSize="xs"
                    color={useColorModeValue("primaryAlpha.300", "whiteAlpha.300")}
                  />
                </VStack> */}
              </HStack>
            </Card>
          )}

          <Card error={errors.amount}>
            <Controller
              control={control}
              render={({ field: { onChange, onBlur, value } }) => (
                <Input
                  variant="unstyled"
                  keyboardType="numeric"
                  placeholderTx="placeholders.enterAmount"
                  value={value}
                  px="0"
                  py="2"
                  size="lg"
                  error={errors.amount}
                  InputRightElement={
                    feeIsLoading ? (
                      <HStack alignItems="center">
                        <Spinner />
                      </HStack>
                    ) : (
                      <Button
                        variant="ghost"
                        size="sm"
                        py={1}
                        _text={{ fontSize: "md" }}
                        textTx="buttons.max"
                        onPress={() => {
                          setTimeout(() => {
                            setMaxAmount(null)
                            setValue("amount", coinBalance.toString(), {
                              shouldValidate: true,
                            })
                          })
                        }}
                      />
                    )
                  }
                  onBlur={onBlur}
                  onChangeText={(value) => onChange(value.replace(",", "."))}
                />
              )}
              name="amount"
              rules={{
                required: true,
                min: 0.00000001,
                max: coinBalance,
                validate: {
                  valid: (value) =>
                    (selectedCoin.name === "jax" ? /^\d+(\.\d{1,4})?$/ : /^\d+(\.\d{1,8})?$/).test(
                      value,
                    ),
                },
              }}
              defaultValue={initialAmount}
            />
            <HStack alignItems="center" mt="1" space={1}>
              <Text
                textTx="texts.availableBalance"
                textTxOptions={{
                  balance: coinBalance,
                  coin: selectedCoin.title,
                }}
                fontSize="xs"
                color={useColorModeValue("primaryAlpha.300", "whiteAlpha.300")}
              />
            </HStack>
          </Card>

          {renderAmountCaption()}

          <Card p="0" mb="2">
            <FeeSelect fees={fees} value={selectedFee} coin={selectedCoin} onChange={selectFee} />
          </Card>
        </VStack>
      </ScreenScrollViewContent>
    </Screen>
  )
})
