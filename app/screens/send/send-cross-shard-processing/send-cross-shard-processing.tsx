import { Observer } from "mobx-react-lite"
import React, { useEffect, useRef, useState } from "react"
import { AppState, Share } from "react-native"

import Clipboard from "@react-native-community/clipboard"
import { StackActions, useNavigation, useRoute } from "@react-navigation/native"

import {
  Box,
  Button,
  Card,
  Center,
  Header,
  HStack,
  Icon,
  IconButton,
  Input,
  Link,
  Modal,
  Screen,
  ScreenScrollViewContent,
  Spinner,
  Text,
  Toast,
  VStack,
} from "../../../components/base"
import { RefundButton } from "../../../components/features"
import { translate } from "../../../i18n"
import { useStores } from "../../../models"
import { StoreError } from "../../../models/extensions"
import { HistoryItemModel } from "../../../models/history-item/history-item"
import { SentCrossShardProcessingScreenRouteProps } from "./send-cross-shard-processing.props"

export const SendCrossShardProcessingScreen = () => {
  const navigation = useNavigation()

  const route = useRoute<SentCrossShardProcessingScreenRouteProps>()
  const { sourceShardID, destinationShardID, transactionHash } = route.params

  const { transactionStore, walletStore, historyStore } = useStores()
  const { address } = walletStore
  const { localHistory } = historyStore

  let historyItem = localHistory.get(sourceShardID.toString()).items.get(transactionHash)

  const appState = useRef(AppState.currentState)
  const [step, setStep] = useState(historyItem.crossShardTemp.step)
  const [hasError, setHasError] = useState(false)
  const [refundTransactionModalIsVisible, setRefundTransactionModalIsVisible] = useState(false)
  const [
    refundTransactionOnErrorModalIsVisible,
    setRefundTransactionOnErrorModalIsVisible,
  ] = useState(false)

  let checkTransactionConfirmationsInterval = null
  let checkTransactionProcessedByEADInterval = null

  const onClose = () => navigation.dispatch(StackActions.popToTop())

  const onRetry = () => {
    proceedCrossShardTransaction()
  }

  const onRefund = (hash: string) => {
    const { sourceAmount, transactionFee } = historyItem

    navigation.reset({
      routes: [
        {
          name: "sentSuccessfully",
          params: {
            address,
            sourceShardID,
            sourceAmount,
            transactionFee,
            transactionHash: hash,
          },
        },
      ],
    })
  }

  const proceedCrossShardTransaction = async () => {
    setHasError(false)
    clearIntervals()

    historyItem = localHistory.get(sourceShardID.toString()).items.get(transactionHash)

    const {
      sourceAmount: amount,
      address,
      transactionFee: fee,
      crossShardTemp: { step: initialStep },
    } = historyItem

    setStep(initialStep)

    if (historyItem.crossShardTemp.step === 0) {
      const { exchangeAgentFee } = historyItem

      const { exchangeAgentUrl } = historyItem.crossShardTemp

      const [
        cstxStep1Data,
        cstxStep1Error,
      ] = await transactionStore.createCrossShardTransactionExchangeInit(
        address,
        sourceShardID,
        destinationShardID,
        amount,
        exchangeAgentFee,
        exchangeAgentUrl,
      )

      if (cstxStep1Error) {
        handleError(cstxStep1Error)
        return
      }

      const {
        exchangeTxUUID,
        exchangeTxSourceShardLock,
        exchangeTxDestinationShardLock,
      } = cstxStep1Data

      historyStore.updateLocal(sourceShardID, transactionHash, {
        refundBlocks: exchangeTxDestinationShardLock,
        crossShardTemp: {
          ...historyItem.crossShardTemp,
          exchangeTxUUID,
          exchangeTxSourceShardLock,
          exchangeTxDestinationShardLock,
        },
      })

      setStep(1)
    }

    if (historyItem.crossShardTemp.step === 1) {
      setHasError(false)

      const {
        exchangeTxSourceShardLock,
        exchangeTxDestinationShardLock,
        exchangeAgentPublicKey,
      } = historyItem.crossShardTemp

      const [
        cstxStep2Data,
        cstxStep2Error,
      ] = await transactionStore.createCrossShardTransactionMultiSigPublish(
        sourceShardID,
        amount,
        fee,
        exchangeAgentPublicKey,
        exchangeTxSourceShardLock,
        exchangeTxDestinationShardLock,
      )

      if (cstxStep2Error) {
        handleError(cstxStep2Error)
        return
      }

      const {
        refundTransactionHex,
        sourceShardFundsLockingTxHash,
        sourceShardMultiSigRedeemHex,
        destinationShardMultiSigRedeemHex,
      } = cstxStep2Data

      console.log("refundTransactionHex", refundTransactionHex)

      historyStore.updateLocal(sourceShardID, transactionHash, {
        refundTransactionHex,
        lockTransactionHash: sourceShardFundsLockingTxHash,
        crossShardTemp: {
          ...historyItem.crossShardTemp,
          sourceShardFundsLockingTxHash,
          sourceShardMultiSigRedeemHex,
          destinationShardMultiSigRedeemHex,
        },
      })

      setStep(2)
    }

    if (historyItem.crossShardTemp.step === 2) {
      setHasError(false)

      const {
        exchangeTxUUID,
        sourceShardMultiSigRedeemHex,
        sourceShardFundsLockingTxHash,
        exchangeAgentUrl,
      } = historyItem.crossShardTemp

      await waitLockInitTransactionConfirmation(sourceShardID, sourceShardFundsLockingTxHash)

      const [, cstxStep3Error] = await transactionStore.createCrossShardTransactionExchangeLock(
        sourceShardMultiSigRedeemHex,
        sourceShardFundsLockingTxHash,
        exchangeTxUUID,
        exchangeAgentUrl,
      )

      if (cstxStep3Error) {
        handleError(cstxStep3Error)
        return
      }

      historyStore.updateLocal(sourceShardID, transactionHash, {
        crossShardTemp: {
          ...historyItem.crossShardTemp,
          isEALocked: true,
        },
      })

      setStep(3)
    }

    if (historyItem.crossShardTemp.step === 3) {
      setHasError(false)

      const { exchangeTxUUID, exchangeAgentUrl } = historyItem.crossShardTemp

      const [
        transactionProcessedByEADData,
        transactionProcessedByEADError,
      ] = await waitTransactionProcessedByEAD(exchangeAgentUrl, exchangeTxUUID)

      if (transactionProcessedByEADError) {
        handleError(transactionProcessedByEADError)
        return
      }

      const {
        rawCSTXHex,
        destinationShardFundsLockTxHash,
        destinationShardFundsLockTxOutIndex,
      } = transactionProcessedByEADData

      const [
        checkInputTransactionData,
        checkInputTransactionError,
      ] = await transactionStore.checkInputTransaction(
        destinationShardID,
        address,
        destinationShardFundsLockTxHash,
        destinationShardFundsLockTxOutIndex,
      )

      if (checkInputTransactionError) {
        handleError(transactionProcessedByEADError)
        return
      }

      const { isUsed } = checkInputTransactionData

      if (isUsed) {
        handleError({ message: translate("notifications.exchangeAgentLockIsUsedDescription") })
        return
      }

      historyStore.updateLocal(sourceShardID, transactionHash, {
        crossShardTemp: {
          ...historyItem.crossShardTemp,
          rawCSTXHex,
          destinationShardFundsLockTxHash,
        },
      })

      setStep(4)
    }

    if (historyItem.crossShardTemp.step === 4) {
      setHasError(false)

      const { exchangeAgentFee } = historyItem
      const {
        exchangeAgentUrl,
        exchangeAgentPublicKey,
        sourceShardMultiSigRedeemHex,
        sourceShardFundsLockingTxHash,
        destinationShardMultiSigRedeemHex,
        rawCSTXHex,
        destinationShardFundsLockTxHash,
      } = historyItem.crossShardTemp

      const [
        cstxStep4Data,
        cstxStep4Error,
      ] = await transactionStore.createCrossShardTransactionSignAndPublish(
        address,
        amount,
        sourceShardID,
        sourceShardMultiSigRedeemHex,
        sourceShardFundsLockingTxHash,
        destinationShardID,
        destinationShardMultiSigRedeemHex,
        exchangeAgentFee,
        exchangeAgentPublicKey,
        rawCSTXHex,
        destinationShardFundsLockTxHash,
      )

      if (cstxStep4Error) {
        handleError(cstxStep4Error)
        return
      }

      const { hash } = cstxStep4Data

      const item = HistoryItemModel.create({
        ...historyStore.history.get(sourceShardID.toString()).items.get(transactionHash),
        refundBlocks: null,
        refundTransactionHex: null,
        crossShardTemp: {
          ...historyStore.history.get(sourceShardID.toString()).items.get(transactionHash)
            .crossShardTemp,
        },
        transactionHash: hash,
        timestamp: new Date().toUTCString(),
      })

      navigation.reset({
        routes: [
          {
            name: "sentSuccessfully",
            params: {
              address,
              sourceShardID,
              destinationShardID,
              sourceAmount: amount,
              destinationAmount: amount - exchangeAgentFee,
              transactionFee: fee,
              transactionHash: hash,
              exchangeAgentFee,
              exchangeAgentUrl,
            },
          },
        ],
      })

      historyStore.removeLocal(sourceShardID, transactionHash)
      item.clearCrossShardTemp()
      historyStore.addOrUpdateLocal(sourceShardID, item)
    }
  }

  const waitLockInitTransactionConfirmation = async (
    sourceShardID: number,
    sourceShardFundsLockingTxHash: string,
  ): Promise<
    [
      {
        confirmations: number
      } | null,
      StoreError | null,
    ]
  > => {
    return await new Promise((resolve) => {
      checkTransactionConfirmationsInterval = setInterval(async () => {
        const { confirmations } = await transactionStore.checkTransactionConfirmations(
          sourceShardID,
          sourceShardFundsLockingTxHash,
        )
        console.log("Source shard funds locking lock TX mined")

        if (confirmations >= 1) {
          clearInterval(checkTransactionConfirmationsInterval)
          resolve([{ confirmations }, null])
        }
      }, 10000)
    })
  }

  const waitTransactionProcessedByEAD = async (
    exchangeAgentUrl: string,
    exchangeTxUUID: string,
  ): Promise<
    [
      {
        confirmations: number
        rawCSTXHex: string
        destinationShardFundsLockTxHash: string
        destinationShardFundsLockTxOutIndex: number
      } | null,
      StoreError | null,
    ]
  > => {
    return await new Promise((resolve) => {
      checkTransactionProcessedByEADInterval = setInterval(async () => {
        const [data, error] = await transactionStore.checkTransactionProcessedByEAD(
          exchangeAgentUrl,
          exchangeTxUUID,
        )

        if (error) {
          resolve([null, error])
        } else {
          const { code, confirmations, confirmationsRequired } = data

          if (confirmations >= confirmationsRequired && code === 0) {
            clearInterval(checkTransactionProcessedByEADInterval)
            historyItem.updateCrossShardTemp({
              exchangeAgentConfirmations: confirmationsRequired,
              exchangeAgentConfirmationsRequired: confirmationsRequired,
            })

            resolve([data, null])
          } else {
            historyItem.updateCrossShardTemp({
              exchangeAgentConfirmations: confirmations,
              exchangeAgentConfirmationsRequired: confirmationsRequired,
            })
          }
        }
      }, 5000)
    })
  }

  const renderStepStatusIcon = (initialStep: number) => {
    const CircleIcon = (
      <Box
        w="22px"
        h="22px"
        borderWidth="2px"
        _light={{
          borderColor: "primaryAlpha.500",
        }}
        _dark={{
          borderColor: "whiteAlpha.500",
        }}
        borderRadius="full"
      />
    )
    for (let i = 0; i <= 4; i++) {
      if (initialStep === i) {
        if (step >= i + 1) {
          return <Icon.Checkmark color="green.500" />
        } else if (step === i || (initialStep === 1 && step <= 1)) {
          if (hasError) return <Icon.Info color="red.500" />

          return <Spinner />
        } else {
          return CircleIcon
        }
      }
    }

    return CircleIcon
  }

  const handleError = (error: StoreError) => {
    console.log("handleError", error)

    if (error.source && error.source.code === 109) {
      Toast.show({
        type: "error",
        text1: translate("notifications.error"),
        text2: translate("notifications.transactionAlreadyPublished"),
      })
    } else {
      Toast.show({
        type: "error",
        text1: translate("notifications.error"),
        text2: `${error.message}`,
      })
    }

    setHasError(true)
    clearIntervals()
  }

  const clearIntervals = () => {
    clearInterval(checkTransactionConfirmationsInterval)
    clearInterval(checkTransactionProcessedByEADInterval)
  }

  const handleAppState = (nextAppState) => {
    if (appState.current.match(/inactive|background/) && nextAppState === "active") {
      console.log("app go to foreground")
      proceedCrossShardTransaction()
    }

    if (appState.current.match(/active/) && nextAppState === "inactive") {
      console.log("app go to background")
      clearIntervals()
    }

    appState.current = nextAppState
  }

  const onCopy = () => {
    const { refundTransactionHex } = historyItem

    Clipboard.setString(refundTransactionHex)
    Toast.show({
      type: "info",
      text1: translate("notifications.copied"),
    })
  }

  const onShare = async () => {
    const { refundTransactionHex } = historyItem

    try {
      const result = await Share.share({
        message: refundTransactionHex,
      })
      if (result.action === Share.sharedAction) {
        Toast.show({
          type: "info",
          text1: translate("notifications.shared"),
        })
      }
    } catch (error) {}
  }

  const renderRefundTransactionModal = () => {
    const {
      refundTransactionHex,
      crossShardTemp: { exchangeTxSourceShardLock },
    } = historyItem

    return (
      <Modal
        isVisible={refundTransactionModalIsVisible}
        HeaderComponent={<Text textTx="titles.copyRefundTransaction" textTransform="uppercase" />}
        FooterComponent={
          <Button
            textTx="buttons.close"
            variant="outline"
            onPress={() => setRefundTransactionModalIsVisible(false)}
          />
        }
        onClosed={() => setRefundTransactionModalIsVisible(false)}
      >
        <VStack space="md">
          <Input editable={false} value={refundTransactionHex} size="lg" />
          <HStack space="xs">
            <Button
              flex={1}
              textTx="buttons.copy"
              variant="outline"
              borderRadius="md"
              endIcon={<Icon.Copy />}
              onPress={onCopy}
            />
            <Button
              flex={1}
              textTx="buttons.share"
              variant="outline"
              borderRadius="md"
              endIcon={<Icon.Share />}
              onPress={onShare}
            />
          </HStack>
          <Text
            textTx="texts.copyRefundTransaction"
            textTxOptions={{
              blocks: exchangeTxSourceShardLock,
              shardID: sourceShardID,
            }}
            align="center"
            secondary
          />
          <Center>
            <Link url="https://peacewallet.io" textTx="buttons.readMore" />
          </Center>
        </VStack>
      </Modal>
    )
  }

  const renderRefundTransactionOnErrorModal = () => {
    const {
      refundTransactionHex,
      crossShardTemp: { exchangeTxSourceShardLock },
    } = historyItem

    return (
      <Modal
        isVisible={refundTransactionOnErrorModalIsVisible}
        HeaderComponent={
          <Text textTx="titles.copyRefundTransactionOnError" textTransform="uppercase" />
        }
        FooterComponent={
          <Button
            textTx="buttons.close"
            variant="outline"
            onPress={() => setRefundTransactionOnErrorModalIsVisible(false)}
          />
        }
        onClosed={() => setRefundTransactionOnErrorModalIsVisible(false)}
      >
        <VStack space="md">
          <Input editable={false} value={refundTransactionHex} size="lg" />
          <HStack space="xs">
            <Button
              flex={1}
              textTx="buttons.copy"
              variant="outline"
              borderRadius="md"
              endIcon={<Icon.Copy />}
              onPress={onCopy}
            />
            <Button
              flex={1}
              textTx="buttons.share"
              variant="outline"
              borderRadius="md"
              endIcon={<Icon.Share />}
              onPress={onShare}
            />
          </HStack>
          <Text
            textTx="texts.copyRefundTransaction"
            textTxOptions={{
              blocks: exchangeTxSourceShardLock,
              shardID: sourceShardID,
            }}
            align="center"
            secondary
          />
          <Center>
            <Link url="https://peacewallet.io" textTx="buttons.readMore" />
          </Center>
        </VStack>
      </Modal>
    )
  }

  useEffect(() => {
    proceedCrossShardTransaction()

    AppState.addEventListener("change", handleAppState)

    return () => {
      clearIntervals()
      AppState.removeEventListener("change", handleAppState)
    }
  }, [])

  useEffect(() => {
    if (hasError && step >= 2) setRefundTransactionOnErrorModalIsVisible(true)
  }, [hasError])

  return (
    <Screen
      header={
        <Header
          hasBackAction={false}
          rightAction={
            (hasError || step >= 4) && (
              <IconButton variant="unstyled" icon={<Icon.Close />} onPress={onClose} />
            )
          }
        />
      }
    >
      {renderRefundTransactionModal()}
      {renderRefundTransactionOnErrorModal()}

      <ScreenScrollViewContent
        footer={
          <HStack alignItems="center" space="xs">
            {hasError && step >= 2 && (
              <RefundButton
                flex={1}
                variant="outline"
                shardID={sourceShardID}
                transactionHex={historyItem.refundTransactionHex}
                onRefund={onRefund}
              />
            )}
            {hasError && <Button flex={1} textTx="buttons.retry" onPress={onRetry} />}
          </HStack>
        }
      >
        <VStack flex={1} space="lg">
          <Text textTx="titles.transactionProcessing" fontSize={28} align="center" />

          <HStack justifyContent="center" px="2">
            <Text textTx="texts.crossShardTransactionProcessing" align="center" secondary />
          </HStack>

          <Card>
            <VStack space="md">
              <HStack alignItems="flex-start" space="sm">
                {renderStepStatusIcon(1)}

                <VStack flex={1} space="xs">
                  <Text
                    textTx={`texts.crossShardTransactionStep1${
                      step >= 2 ? "Completed" : "Pending"
                    }`}
                    flex={1}
                    _light={{
                      color: step >= 2 ? "primary.500" : "primaryAlpha.500",
                    }}
                    _dark={{
                      color: step >= 2 ? "white" : "whiteAlpha.500",
                    }}
                  />

                  <HStack justifyContent="center">
                    <Button
                      textTx="buttons.showRefundTransaction"
                      size="sm"
                      variant="outline"
                      isDisabled={step < 2}
                      onPress={() => setRefundTransactionModalIsVisible(true)}
                    />
                  </HStack>
                </VStack>
              </HStack>

              <HStack alignItems="flex-start" space="sm">
                {renderStepStatusIcon(2)}
                <Text
                  textTx={`texts.crossShardTransactionStep2${step >= 3 ? "Completed" : "Pending"}`}
                  flex={1}
                  _light={{
                    color: step >= 3 ? "primary.500" : "primaryAlpha.500",
                  }}
                  _dark={{
                    color: step >= 3 ? "white" : "whiteAlpha.500",
                  }}
                />
              </HStack>

              <HStack alignItems="flex-start" space="sm">
                {renderStepStatusIcon(3)}

                <VStack flex={1}>
                  <Text
                    textTx={`texts.crossShardTransactionStep3${
                      step >= 4 ? "Completed" : "Pending"
                    }`}
                    _light={{
                      color: step >= 4 ? "primary.500" : "primaryAlpha.500",
                    }}
                    _dark={{
                      color: step >= 4 ? "white" : "whiteAlpha.500",
                    }}
                  />

                  <HStack alignItems="center">
                    <Text
                      textTx="labels.confirmations"
                      secondary
                      _light={{
                        color: step >= 4 ? "primary.500" : "primaryAlpha.500",
                      }}
                      _dark={{
                        color: step >= 4 ? "white" : "whiteAlpha.500",
                      }}
                      fontWeight="400"
                    />
                    <Observer>
                      {() => (
                        <Text
                          text={`: ${historyItem.crossShardTemp.exchangeAgentConfirmations}/${historyItem.crossShardTemp.exchangeAgentConfirmationsRequired}`}
                          _light={{
                            color: step >= 4 ? "primary.500" : "primaryAlpha.500",
                          }}
                          _dark={{
                            color: step >= 4 ? "white" : "whiteAlpha.500",
                          }}
                          secondary
                          fontWeight="400"
                        />
                      )}
                    </Observer>
                  </HStack>
                </VStack>
              </HStack>

              <HStack alignItems="flex-start" space="sm">
                {renderStepStatusIcon(4)}
                <Text
                  textTx={`texts.crossShardTransactionStep4${step >= 5 ? "Completed" : "Pending"}`}
                  flex={1}
                  _light={{
                    color: step >= 5 ? "primary.500" : "primaryAlpha.500",
                  }}
                  _dark={{
                    color: step >= 5 ? "white" : "whiteAlpha.500",
                  }}
                />
              </HStack>
            </VStack>
          </Card>
        </VStack>
      </ScreenScrollViewContent>
    </Screen>
  )
}
