export type SendCrossShardProcessingScreenParams = {
  sourceShardID: number
  destinationShardID: number
  transactionHash: string
}
