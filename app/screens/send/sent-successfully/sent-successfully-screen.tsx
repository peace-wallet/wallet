import React from "react"
import { Linking, Share } from "react-native"

import Clipboard from "@react-native-community/clipboard"
import { StackActions, useNavigation, useRoute } from "@react-navigation/native"

import {
  Box,
  Button,
  Card,
  Header,
  HStack,
  Icon,
  IconButton,
  Screen,
  ScreenScrollViewContent,
  Toast,
  VStack,
} from "../../../components/base"
import { TransactionDetails } from "../../../components/features"
import { translate } from "../../../i18n"
import { useStores } from "../../../models"
import { SentSuccessfullyScreenRouteProps } from "./sent-successfully-screen.props"

export const SentSuccessfullyScreen = () => {
  const navigation = useNavigation()

  const route = useRoute<SentSuccessfullyScreenRouteProps>()
  const {
    address,
    sourceShardID,
    destinationShardID,
    sourceAmount,
    destinationAmount,
    transactionFee,
    transactionHash,
    exchangeAgentFee,
    exchangeAgentUrl,
  } = route.params

  const { coinStore, networkName } = useStores()
  const { selectedCoin } = coinStore

  const url =
    selectedCoin.name === "btc"
      ? `https://www.blockchain.com/en/search?search=${transactionHash}`
      : `https://explore.jax.net/shards/${sourceShardID}/transactions/${transactionHash}?network=${networkName}`

  const onOpen = async () => {
    const supported = await Linking.canOpenURL(url)

    if (supported) {
      await Linking.openURL(url)
    } else {
      Toast.show({
        type: "error",
        text1: "Don't know how to open this URL",
      })
    }
  }

  const onCopy = () => {
    Clipboard.setString(transactionHash)
    Toast.show({
      type: "info",
      text1: translate("notifications.copied"),
      text2: transactionHash,
    })
  }

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `Jax Explorer | ${transactionHash} \n\n${url}`,
      })
      if (result.action === Share.sharedAction) {
        Toast.show({
          type: "info",
          text1: translate("notifications.shared"),
          text2: transactionHash,
        })
      }
    } catch (error) {}
  }

  const onClose = () => navigation.dispatch(StackActions.popToTop())

  return (
    <Screen
      header={
        <Header
          titleTx="titles.transactionCreated"
          hasBackAction={false}
          rightAction={<IconButton variant="unstyled" icon={<Icon.Close />} onPress={onClose} />}
        />
      }
    >
      <ScreenScrollViewContent footer={<Button textTx="buttons.openInExplorer" onPress={onOpen} />}>
        <Card p="2">
          <VStack space={6}>
            <Box p="2">
              <TransactionDetails
                coin={selectedCoin}
                sourceAmount={sourceAmount}
                destinationAmount={destinationAmount}
                address={address}
                sourceShardID={sourceShardID}
                destinationShardID={destinationShardID}
                transactionFee={transactionFee}
                hash={transactionHash}
                exchangeAgentFee={exchangeAgentFee}
                exchangeAgentUrl={exchangeAgentUrl}
              />
            </Box>

            <HStack space="xs">
              <Button
                flex={1}
                textTx="buttons.copy"
                variant="outline"
                borderRadius="md"
                endIcon={<Icon.Copy />}
                onPress={onCopy}
              />
              <Button
                flex={1}
                textTx="buttons.share"
                variant="outline"
                borderRadius="md"
                endIcon={<Icon.Share />}
                onPress={onShare}
              />
            </HStack>
          </VStack>
        </Card>
      </ScreenScrollViewContent>
    </Screen>
  )
}
