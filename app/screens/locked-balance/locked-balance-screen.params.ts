import { Coin } from "../../models"

export type LockedBalanceScreenParams = {
  coin: Coin
}
