import { RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { BalanceNavigatorParamList } from "../../navigators/stacks"

export type LockedBalanceScreenRouteProps = RouteProp<BalanceNavigatorParamList, "lockedBalance">
export type LockedBalanceScreenNavigationProps = StackNavigationProp<
  BalanceNavigatorParamList,
  "lockedBalance"
>
