import React from "react"
import { Linking, StyleSheet } from "react-native"
import {
  Card,
  Divider,
  Header,
  HStack,
  Icon,
  Pressable,
  Screen,
  ScreenContent,
  Text,
  VStack,
} from "../../components/base"
import { useTabBarVisible } from "../../hooks"

export const SupportScreen = () => {
  const navigateToKnowledgeBase = async () => {
    const url = "https://jax-support.freshdesk.com/support/home"
    const supported = await Linking.canOpenURL(url)

    if (supported) {
      await Linking.openURL(url)
    }
  }

  useTabBarVisible(false)

  return (
    <Screen header={<Header titleTx="titles.support" />}>
      <ScreenContent>
        <Card>
          <HStack>
            <VStack flex={1} space="md">
              <HStack alignItems="center">
                <Icon.Phone />
                <VStack ml={3}>
                  <Text
                    text="Phone"
                    _light={{ color: "primaryAlpha.500" }}
                    _dark={{ color: "whiteAlpha.500" }}
                  />
                  <Text text="+380508242618" />
                </VStack>
              </HStack>
              <Divider />
              <HStack alignItems="center">
                <Icon.Send />
                <VStack ml={3}>
                  <Text
                    text="Email"
                    _light={{ color: "primaryAlpha.500" }}
                    _dark={{ color: "whiteAlpha.500" }}
                  />
                  <Text text="support@peacewallet.io" />
                </VStack>
              </HStack>
              <Divider />
              <Pressable onPress={navigateToKnowledgeBase}>
                <HStack py="5" alignItems="center" justifyContent="space-between">
                  <Text text="Knowledge base" />
                  <Icon.ChevronRight
                    _light={{ color: "primaryAlpha.200" }}
                    _dark={{ color: "whiteAlpha.200" }}
                  />
                </HStack>
              </Pressable>
            </VStack>
          </HStack>
        </Card>
      </ScreenContent>
    </Screen>
  )
}

const styles = StyleSheet.create({})
