import { Platform } from "react-native"

export const sizing = {
  headerHeight: 48,
  bottomTabBarHeight: Platform.OS === "ios" ? 84 : 58,
}
